FROM node:18-alpine as build-step
#RUN node --version
#RUN npm -v
#RUN apk add --no-cache python3 g++ make jq
RUN apk add --no-cache jq
#FROM node:latest as build-step
RUN mkdir -p /app
WORKDIR /app
COPY package.json package-lock.json /app/

#RUN yarn global add @angular/cli --non-interactive --network-timeout 1000000
#RUN ng config -g cli.packageManager yarn
#RUN yarn install --frozen-lockfile --network-timeout 1000000
RUN npm install -timeout=8000000
COPY . /app
#RUN yarn run build --configuration production
RUN jq 'to_entries | map_values({ (.key) : ("$" + .key) }) | reduce .[] as $item ({}; . + $item)' ./src/config.json > ./src/config.tmp.json && mv ./src/config.tmp.json ./src/config.json
RUN npm run build --omit=dev


FROM nginx:1.23.1-alpine
#FROM nginx:1.21.1-alpine
COPY --from=build-step /app/build /usr/share/nginx/html

COPY ./nginx.d/nginx.conf /etc/nginx/nginx.conf
COPY ./nginx.d/proxy.conf /etc/nginx/nginx.d/proxy.conf
EXPOSE 80

ENV JSFOLDER=/usr/share/nginx/html/static/js/*.js
COPY ./entrypoint.sh /usr/bin/entrypoint.sh
RUN chmod +x /usr/bin/entrypoint.sh

ENTRYPOINT ["/usr/bin/entrypoint.sh"]

declare module 'eventsourcemock' {
    interface EventSourceMockOptions {
        url?: string;
        event?: string;
        data?: any;
        retry?: number;
    }

    class EventSourceMock {
        constructor(url?: string, eventSourceInitDict?: any);

        addEventListener(type: string, listener: EventListenerOrEventListenerObject, options?: boolean | AddEventListenerOptions): void;

        removeEventListener(type: string, listener: EventListenerOrEventListenerObject, options?: boolean | EventListenerOptions): void;

        dispatchEvent(event: Event): boolean;

        connect(open?: boolean): void;

        close(): void;

        emit(eventName: string, data?: any): void;

        setRetryTime(time: number): void;

        setEventId(id: string): void;

        on(event: string, callback: () => void): void;

        emitEvent(event: string, data?: any, eventId?: string): void;

        simulateEvent(event?: string, data?: any): void;
    }

    export default EventSourceMock;
}

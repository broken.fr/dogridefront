import { resources } from '../i18n/config';

declare module 'i18next' {
    interface CustomTypeOptions {
        defaultNS: typeof resources['fr'];
        resources: typeof resources['fr'];
        returnNull: false;
    }
}
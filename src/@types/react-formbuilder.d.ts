declare module "@elevenlabs/react-formbuilder" {
  import React from "react";

  import { FormikHelpers } from "formik/dist/types";
  import { FormikProps, FormikValues } from "formik";

  interface FieldObject {
    fieldType: React.ComponentType<any>;

    [key: string]: any;
  }

  interface FieldComponents {
    Label: React.FC<any>;
    Field: React.FC<any>;
    Errors: React.FC<any>;
  }

  type Fields = {
    [key: string]: FieldObject;
  };

  interface Validation {
    (value: any, allValues?: any, props?: any): string | undefined | null;
  }

  interface Validations {
    [key: string]: Validation;
  }

  interface ValidationMessages {
    [key: string]: string;
  }

  interface Theme {
    Form: React.ComponentType<any>;
    Row: React.ComponentType<any>;
    Label: React.ComponentType<any>;
  }

  interface FormBuilderProps {
    handleSubmit?: (values?: any, formikBag?: any) => void;
    children?: React.ReactNode;

    [key: string]: any;
  }

  interface FormikPropsWrapper {
    formRef?: React.RefObject<any>;
    onSubmit: (
      values: FormikValues,
      formikBag: FormikHelpers<FormikValues>
    ) => void;
    initialValues?: any;
    render?: (formikProps: FormikProps<any>) => React.ReactNode;
    hasErrors?: (hasErrors: boolean) => void;

    [key: string]: any;
  }

  class FormBuilder {
    _fields: Fields;
    _fieldComponents: {
      [key: string]: FieldComponents;
    };
    _translate: any;
    _theme: Theme | null;
    _initialValues: any;
    _validators: {
      [key: string]: Validation;
    };
    _validations: Validations;
    _validatorsSpec: any;
    add: (name: string, fieldType: string, _ref6: any) => FormBuilder;
    Fields: any;

    // Formik(props: FormikPropsWrapper): ComponentType<{
    //   className?: string;
    //   onSubmit?: (payload: any, formikProps: any) => void;
    //   render?: (formikProps: FormikProps<any>) => JSX.Element;
    // }>;

    Formik(props: FormikPropsWrapper): JSX.Element;

    setTheme(theme: Theme): this;

    setTranslate(translate: any): this;

    setValidations(validations: Validations): this;

    createForm(): this;

    Label(name: string, props: any): React.ReactNode;

    Field(name: string, props: any, notFastField?: boolean): React.ReactNode;

    Errors(name: string, props: any): React.ReactNode;

    Form(props: FormBuilderProps): JSX.Element;

    _validationMessages(): ValidationMessages;

    _validate(values: any): any;

    _render(props: any): React.ReactNode;

    _buildFieldComponent(name: string, notFastField?: boolean): void;
  }

  export default FormBuilder;
}

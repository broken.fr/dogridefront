import React from 'react';
import {render, screen} from '@testing-library/react';
import { BrowserRouter } from 'react-router-dom';
import { ThemeProvider } from '@mui/material';
import { createTheme } from '@mui/material/styles';
import App from './App';
import {User} from "./interfaces/user";
import {mocked} from "jest-mock";
import {Auth0ContextInterface, useAuth0} from "@auth0/auth0-react";
import '@testing-library/jest-dom'
import MatchMediaMock from 'jest-matchmedia-mock';


const user: User = {
  email: "johndoe@me.com",
  email_verified: true,
  family_name: "Doe",
  username: "johndoe"
};

jest.mock("@auth0/auth0-react");
jest.mock('../assets/img/logo.png', () => 'logo.png');

const mockedUseAuth0 = mocked(useAuth0);
const renderWithProviders = (ui: React.ReactElement) => {
  const theme = createTheme();
  return render(<BrowserRouter><ThemeProvider theme={theme}>{ui}</ThemeProvider></BrowserRouter>);
};


const mock: Auth0ContextInterface<User> = {
  buildAuthorizeUrl: jest.fn(),
  buildLogoutUrl: jest.fn(),
  handleRedirectCallback: jest.fn(),
  isAuthenticated: true,
  user,
  logout: jest.fn(),
  loginWithRedirect: jest.fn(),
  getAccessTokenWithPopup: jest.fn(),
  getAccessTokenSilently: jest.fn(),
  getIdTokenClaims: jest.fn(),
  loginWithPopup: jest.fn(),
  isLoading: false
};
let matchMedia: MatchMediaMock;
// beforeAll(() => {
//   MatchMedia.init();
//
// });
//
// afterAll(() => {
//   MatchMedia.destroy();
// });
describe('App component', () => {
  beforeAll(() => {
    matchMedia = new MatchMediaMock();
  });

  afterEach(() => {
    matchMedia.clear();
  });
  beforeEach(() => {
    mockedUseAuth0.mockReturnValue(mock);
  });
  test('renders without crashing', () => {
    renderWithProviders(<App />);
  });

  test('renders NavBar component', () => {
    renderWithProviders(<App />);
    const navBarElement = screen.getByRole('banner');
    expect(navBarElement).toBeInTheDocument();
  });

  // test('renders correct routes', async () => {
  //   renderWithProviders(<App />);
  //   const routes = [
  //     { path: '/user/profile', component: 'Profile' },
  //     { path: '/user/profile/create', component: 'Create' },
  //     { path: '/dog/create', component: 'CreateDog' },
  //     { path: '/dog/list', component: 'ListDogs' },
  //     { path: '/dog/profile/:id', component: 'ProfileDog' },
  //     { path: '/walk/create', component: 'CreateWalk' },
  //     { path: '/', component: 'Home' },
  //   ];
  //
  //   for (const route of routes) {
  //     const { component } = route;
  //     expect(screen.getByText(component)).toBeInTheDocument();
  //   }
  // });

  test('applies theme correctly', () => {
    renderWithProviders(<App />);
    const body = document.body;
    const computedStyle = getComputedStyle(body);
    const mainTextColor = computedStyle.getPropertyValue('color').trim();
    let textColor;
    if (window.matchMedia('(prefers-color-scheme: dark)').matches) {
      textColor = '#f5f5f5';
    } else {
      textColor = 'rgb(2, 2, 2)';
    }
      expect(mainTextColor).toEqual(textColor); // expected main text color in light theme
  });
});
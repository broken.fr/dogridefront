import React, { Suspense, useEffect } from "react";
import { Outlet, Route, Routes } from "react-router-dom";
import NavBar from "./components/NavBar";
import Home from "./pages/Home";
import {
  Container,
  createTheme,
  CssBaseline,
  Grid,
  ThemeProvider,
  useMediaQuery,
} from "@mui/material";
import Box from "@mui/material/Box";
import { useAuth0 } from "@auth0/auth0-react";
import { ToastContainer } from "react-toastify";
import { StyledEngineProvider } from "@mui/material/styles";
import "react-toastify/ReactToastify.min.css";
import { frFR, LocalizationProvider } from "@mui/x-date-pickers";
import { AdapterMoment } from "@mui/x-date-pickers/AdapterMoment";
import "moment/locale/fr";
import { ThemeContext } from "./lib/ThemeContext";
import DelayedFallback from "./components/assets/DelayedFallback";
import CreateWalk from "./pages/walk/CreateWalk";
import Profile from "./pages/user/Profile";
import Edit from "./pages/user/Edit";
import CreateDog from "./pages/dog/CreateDog";
import ListDogs from "./pages/dog/ListDogs";
import ProfileDog from "./pages/dog/ProfileDog";
import { ViewWalk } from "./pages/walk/ViewWalk";
import { SubscribeWalk } from "./pages/walk/SubscribeWalk";
import { ReviewWalk } from "./pages/walk/ReviewWalk";
import EditWalk from "./pages/walk/EditWalk";
import EditDog from "./pages/dog/EditDog";
import LegalNotice from "./pages/footer/LegalNotice";
import RecommendationsWalk from "./pages/footer/RecommendationsWalk";
import Footer from "./components/Footer";
import ProfileById from "./pages/user/ProfileById";
import DogRideTeam from "./pages/footer/DogRideTeam";

const locale = "fr-FR";

function App() {
  const [mode, setMode] = React.useState<"light" | "dark">(
    useMediaQuery("(prefers-color-scheme: dark)") ? "dark" : "light"
  );
  const colorMode = { mode, setMode };

  const theme = React.useMemo(
    () =>
      createTheme({
        palette: {
          mode,
          ...(mode === "light"
            ? {
                primary: {
                  main: "#004aad",
                },
                secondary: {
                  main: "#ffde59",
                },
                background: {
                  paper: "#f5f5f5",
                  default: "#ffde59",
                },

                text: {
                  primary: "rgb(2, 2, 2)",
                },
                color: {
                  red: "#b71c1c",
                  orange: "#ef6c00",
                  yellow: "#ffea00",
                  green: "#4caf50",
                  blue: "#0091ea",
                  indigo: "#1a237e",
                  purple: "#673ab7",
                },
              }
            : {
                primary: {
                  main: "#ffde59",
                },
                secondary: {
                  main: "#004aad",
                },
                background: {
                  paper: "#606060",
                  default: "#424242",
                },
                text: {
                  primary: "#f5f5f5",
                },
                color: {
                  red: "#b71c1c",
                  orange: "#ef6c00",
                  yellow: "#ffea00",
                  green: "#4caf50",
                  blue: "#0091ea",
                  indigo: "#1a237e",
                  purple: "#673ab7",
                },
              }),
        },
        typography: {
          fontFamily: "LatoLight, Arial, Helvetica, sans-seri",
          h1: {
            fontFamily: "Adumu",
            fontSize: "35px",
            fontWeight: "300",
            lineHeight: "1.167",
          },
          h2: {
            fontFamily: "Adumu",
            fontSize: "30px",
            fontWeight: "300",
            lineHeight: "1.167",
          },
        },
        components: {
          MuiCssBaseline: {
            styleOverrides: `
                            @font-face {
                              font-family: 'Adumu';
                              src: url('../assets/font/Adumu.ttf');
                            },
                            @font-face {
                              font-family: 'LatoLight';
                              src: url('../assets/font/LatoLight.ttf');
                            }
                          `,
          },
        },
      }),
    [mode]
  );
  const { isLoading, isAuthenticated } = useAuth0();
  useEffect(() => {
    if (isLoading || isAuthenticated) {
      return;
    }
  }, [isLoading, isAuthenticated]);

  return (
    <Routes>
      <Route
        path="/"
        element={
          <ThemeContext.Provider value={colorMode}>
            <ThemeProvider theme={theme}>
              <LocalizationProvider
                dateAdapter={AdapterMoment}
                adapterLocale={locale}
                localeText={
                  frFR.components.MuiLocalizationProvider.defaultProps
                    .localeText
                }
              >
                <StyledEngineProvider injectFirst>
                  <CssBaseline />
                  <NavBar />
                  <Grid
                    container
                    style={{
                      backgroundImage:
                        "url('../assets/img/backgoundNatur.jpeg')",
                      backgroundPosition: "center",
                      backgroundRepeat: "no-repeat",
                      minHeight: "750px",
                    }}
                  >
                    <Grid item xs={12}>
                      {/*<CustomBackdrop>*/}
                      <Box
                        m={"5%"}
                        style={{
                          padding: "1%",
                          borderRadius: "30px",
                          backgroundColor: theme.palette.background.paper,
                        }}
                      >
                        <Container>
                          <Suspense fallback={<DelayedFallback />}>
                            <Outlet />
                          </Suspense>
                        </Container>
                      </Box>
                    </Grid>
                    {/*</CustomBackdrop>*/}
                  </Grid>
                  <Footer />
                  <ToastContainer
                    position="bottom-right"
                    autoClose={5000}
                    hideProgressBar={false}
                    newestOnTop
                    closeOnClick
                    rtl={false}
                    pauseOnFocusLoss
                    draggable
                    pauseOnHover
                    theme={"colored"}
                  />
                </StyledEngineProvider>
              </LocalizationProvider>
            </ThemeProvider>
          </ThemeContext.Provider>
        }
      >
        <Route path="user/profile" element={<Profile />} />
        <Route path="user/profile/:id" element={<ProfileById />} />
        <Route path="user/profile/edit" element={<Edit />} />
        <Route path="dog/create" element={<CreateDog />} />
        <Route path="dog/list" element={<ListDogs />} />
        <Route path="dog/profile/:id" element={<ProfileDog />} />
        <Route path="dog/edit/:id" element={<EditDog />} />
        <Route path="walk/create" element={<CreateWalk />} />
        <Route path="walk/view/:id" element={<ViewWalk />} />
        <Route path="walk/subscribe/:id" element={<SubscribeWalk />} />
        <Route path="walk/review/:id" element={<ReviewWalk />} />
        <Route path="walk/edit/:id" element={<EditWalk />} />
        <Route path="footer/legalNotice" element={<LegalNotice />} />
        <Route
          path="footer/RecommendationsWalk"
          element={<RecommendationsWalk />}
        />
        <Route path="footer/DogRideTeam" element={<DogRideTeam />} />
        <Route index element={<Home />} />
      </Route>
    </Routes>
  );
}

export default App;

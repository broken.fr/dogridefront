import React from 'react';
import {render} from '@testing-library/react';
import Auth0Provider from './Auth0Provider';


import {MemoryRouter} from 'react-router-dom';
import {Auth0Provider as AOP, Auth0ProviderOptions} from "@auth0/auth0-react";
import Config from "./config.json";

jest.mock("@auth0/auth0-react");

describe('Auth0Provider', () => {
    const domain = Config.REACT_APP_AUTH0_DOMAIN;
    const clientId = Config.REACT_APP_AUTH0_CLIENT_ID;
    const options: Auth0ProviderOptions = {
        domain: domain,
        clientId: clientId,
        redirectUri: 'http://localhost',
        connection: 'doggobox',
        audience: 'https://doggobox.fr',
        useRefreshTokens: true,
        cacheLocation: "localstorage"
    };

    beforeEach(() => {
        (AOP as jest.MockedFunction<typeof AOP>).mockClear();
    });

    it('renders without crashing', () => {
        render(
            <MemoryRouter>
                <Auth0Provider {...options}>
                    <div>Test</div>
                </Auth0Provider>
            </MemoryRouter>
        );

        expect(AOP).toHaveBeenCalledTimes(1);

        const expectedOptions = {
            ...options,
            onRedirectCallback: () => {},
            children: <div>Test</div>
        };
        const receivedOptions = JSON.parse(JSON.stringify((AOP as jest.MockedFunction<typeof AOP>).mock.calls[0][0]));
        expect(receivedOptions).toEqual(JSON.parse(JSON.stringify(expectedOptions)));
    });
});

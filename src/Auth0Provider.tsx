import React from "react";
import { Auth0Provider as AOP } from "@auth0/auth0-react";
import { useNavigate } from "react-router-dom";
import Config from "./config.json";

const Auth0Provider: React.FC<{ children: JSX.Element }> = ({ children }) => {
  const domain = Config.REACT_APP_AUTH0_DOMAIN;
  const clientId = Config.REACT_APP_AUTH0_CLIENT_ID;
  const audience = Config.REACT_APP_AUTH0_AUDIENCE;
  const connection = Config.REACT_APP_AUTH0_CONNECTION;
  const navigate = useNavigate();

  const onRedirectCallback = (appState: any) => {
    navigate(appState?.returnTo || window.location.pathname);
  };
  return (
    <AOP
      domain={domain}
      clientId={clientId}
      redirectUri={window.location.origin}
      connection={connection}
      audience={audience}
      useRefreshTokens={true}
      cacheLocation="localstorage"
      onRedirectCallback={onRedirectCallback}
    >
      {children}
    </AOP>
  );
};
export default Auth0Provider;

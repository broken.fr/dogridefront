import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';
import {Link} from 'react-router-dom';
import { Button, Chip, Grid} from '@mui/material';
import Config from "../config.json";
import Stack from '@mui/material/Stack';
import FemaleIcon from '@mui/icons-material/Female';
import MaleIcon from '@mui/icons-material/Male';
import {Dog} from "../interfaces/dog";
import {Breed} from "../interfaces/breed";
import {MediaObject} from "../interfaces/mediaobject";
import {useTranslation} from "react-i18next";
import Skeleton from "@mui/material/Skeleton";
import {User} from "../interfaces/user";
import {UserExtraData} from "../interfaces/userextradata";
import moment from "moment/moment";


const CardDogs = ({ user }:{ user: User }) => {

    const dogs = ((user?.user_extra_data as UserExtraData)?.dogs as Dog[])
    const {t} = useTranslation();
    function gender(sex: string) {
        if (sex === 'female') {
            return (<><Chip label={<FemaleIcon/>} style={{backgroundColor: "#ec407a"}}/></>);
        }
        return (<><Chip label={<MaleIcon/>} style={{backgroundColor: "#00bcd4"}}/></>)
    }
    function birthdate (age: any) {
        let years = moment().diff(age, 'years');
        let month = moment().diff(age, 'month');
        let brith = "";
        if (years > 1){
            brith = years + " ans"
        }else {
            brith = month + " mois"
        }
        return brith;
    }

    return (
        <>
        <Typography variant={"h2"} textAlign={"center"} m={5} fontSize={"30px"}>La meute de {user.username} 🐕</Typography>
        <Grid container justifyContent="center" alignItems={'stretch'} spacing={3}>

            {(dogs?.map(
                    (dog: Dog, index) => {
                        return (
                            <Grid key={index} item xs={12} sm={6} md={4}>
                                <Card sx={{
                                    height: '100%',
                                    display: 'flex',
                                    flexDirection: 'column',
                                    justifyContent: 'space-between'
                                }}>
                                    <CardMedia
                                        component="img"
                                        alt={'image de profil de ' + dog.name}
                                        height="250px"
                                        image={Config.REACT_APP_DOGGOBOX_URL + (dog?.image as MediaObject)?.content_url}
                                    />
                                    <CardContent>
                                        <Typography gutterBottom variant="h3">
                                            {dog.name} {gender(dog.gender)}
                                        </Typography>
                                        <Stack spacing={1}>
                                            <Grid container spacing={1}>
                                                <Grid item>
                                                    <Chip label={(dog.breed as Breed).name} color="primary"/>
                                                </Grid>
                                                <Grid item>
                                                    <Chip label={birthdate(dog?.birthdate)} color="primary"/>
                                                </Grid>
                                                <Grid item>
                                                    <Chip label={t(`dog.category.${dog.category}`)} color="primary"/>
                                                </Grid>
                                            </Grid>
                                        </Stack>
                                    </CardContent>
                                    <CardActions>
                                        <Button component={Link} to={'/dog/profile/' + dog.id}>Voir le
                                            profil</Button>
                                    </CardActions>
                                </Card>
                            </Grid>
                        )
                    })
            )}
            {dogs ? null : [...Array(3)].map((_, i) => (
                <Grid key={i} item xs={12} sm={6} md={4}>
                    <Card sx={{maxWidth: 345, m: 2}}>
                        <Skeleton sx={{height: 190}} animation="wave" variant="rectangular"/>
                        <CardContent>
                            <>
                                <Skeleton animation="wave" height={10} style={{marginBottom: 6}}/>
                                <Skeleton animation="wave" height={10} width="80%"/>
                            </>
                        </CardContent>
                    </Card>
                </Grid>
            ))}
        </Grid>
        </>
    )
};

export default CardDogs;
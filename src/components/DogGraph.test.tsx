import { render } from "@testing-library/react";
import { DogGraph } from "./DogGraph";
import { Dog } from "../interfaces/dog";
import moment from "moment";

global.window.ResizeObserver = jest.fn(() => ({
  observe: jest.fn(),
  unobserve: jest.fn(),
  disconnect: jest.fn(),
}));

const dog: Dog = {
  return_response: 3,
  stop_response: 4,
  runner: 2,
  sharing: 5,
  playfullness: 4,
  swimmer: 3,
  fighter: 1,
  sniffer: 2,
  name: "Buddy",
  amity_puppies: 3,
  amity_females: 4,
  energy_level: 2,
  amity_males: 5,
  category: "category_1",
  birthdate: moment("2021-01-01"),
  breed: "breed_1",
  adventurous_temper: true,
  brawler_temper: true,
  grumpy_temper: true,
  peaceful_temper: true,
  independent_temper: true,
  user_extra_data: {
    travel_area: 1,
    positive_way: 1,
    natural_way: 1,
    traditional_way: 1,
    user: "user",
  },
  sterilized: true,
  brutal_temper: true,
  mild_temper: true,
  shy_temper: true,
  gender: "female",
};

describe("DogGraph", () => {
  it("should render without error", () => {
    render(<DogGraph dog={dog} />);
  });
});

import React from "react";
import { useTheme } from "@mui/material";
import {
  PolarAngleAxis,
  PolarGrid,
  PolarRadiusAxis,
  Radar,
  RadarChart,
  ResponsiveContainer,
} from "recharts";
import { useTranslation } from "react-i18next";
import { Dog } from "../interfaces/dog";

interface ChartData {
  name: string;
  value: number;
  fullMark: number;
}

export const DogGraph = ({ dog }: { dog: Dog }) => {
  const theme = useTheme();
  const { t } = useTranslation();
  const data: ChartData[] = [
    { name: t("dog.runner"), value: dog?.runner, fullMark: 5 },
    { name: t("dog.playfullness"), value: dog?.playfullness, fullMark: 5 },
    { name: t("dog.swimmer"), value: dog?.swimmer, fullMark: 5 },
    { name: t("dog.fighter"), value: dog?.fighter, fullMark: 5 },
    { name: t("dog.sniffer"), value: dog?.sniffer, fullMark: 5 },
  ];
  const startColor = "#0d6b60";
  const stopColor = "#17a295";

  return (
    <ResponsiveContainer width="100%" height="100%">
      <RadarChart cx="50%" cy="50%" outerRadius="70%" data={data}>
        <defs>
          <linearGradient id="grad1" x1="0%" y1="0%" x2="100%" y2="0%">
            <stop offset="0%" stopColor={startColor} />
            <stop offset="100%" stopColor={stopColor} />
          </linearGradient>
        </defs>
        <PolarGrid />
        <PolarRadiusAxis domain={[0, 5]} display={"none"} tickCount={6} />
        <PolarAngleAxis dataKey="name" />
        <Radar
          name="Type d'education"
          dataKey="value"
          stroke={theme.palette.primary.main}
          fill="url(#grad1)"
          fillOpacity={0.6}
          isAnimationActive={true}
        />
      </RadarChart>
    </ResponsiveContainer>
  );
};

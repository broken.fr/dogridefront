import {Button, Grid, Typography, useTheme} from "@mui/material";
import {Link} from "react-router-dom";
import FacebookIcon from '@mui/icons-material/Facebook';
import InstagramIcon from '@mui/icons-material/Instagram';

const Footer = () => {
    const theme = useTheme();
    return (
        <Grid container textAlign="center"
              style={{
                  backgroundColor: theme.palette.background.default,
                  color: theme.palette.text.primary,
                  paddingTop: '20px',
                  paddingBottom: '20px',
                  bottom: 0,
              }}>
            <Grid item xs={12} mt={1}>
                <img src={'../assets/img/logoDogRide.png'} alt={'logo de Dog Ride'} height={"100px"}/>
            </Grid>
            <Grid item xs={12} mt={1}>
                <Button component={Link} to="https://www.facebook.com/profile.php?id=100092503497410">
                    <FacebookIcon fontSize="large" color="primary"/>
                </Button>
                <Button component={Link} to="https://www.instagram.com/dogride.fr/">
                    <InstagramIcon fontSize="large" color="primary"/>
                </Button>
            </Grid>
            <Grid item xs={12} mt={1}>
                <Typography component={Link} to="footer/RecommendationsWalk" color={theme.palette.text.primary}>Préconisations pour une Balade</Typography>
                <Typography ml={2} component={Link} to="footer/LegalNotice" color={theme.palette.text.primary}>Mentions légales</Typography>
                <Typography ml={2} component={Link} to="/footer/DogRideTeam" color={theme.palette.text.primary}>L'équipe Dog Ride</Typography>
            </Grid>
        </Grid>
    )
};
export default Footer;
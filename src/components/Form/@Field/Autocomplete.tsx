import { debounce, pick } from "lodash";
import React, { useCallback, useEffect, useRef } from "react";
import { Autocomplete as MuiAutocomplete, TextField } from "@mui/material";
import { useTranslation } from "react-i18next";
import { FormikContextType } from "formik/dist/types";
import { FormikValues } from "formik";
import { toast } from "react-toastify";
import { AxiosConnection } from "../../../lib/AxiosConnection";

function usePrevious(value: any) {
  const ref = useRef();
  useEffect(() => {
    ref.current = value; //assign the value of ref to the argument
  }, [value]); //this code will run when the value of 'value' changes
  return ref.current; //in the end, return the current ref value.
}

export default function Autocomplete(props: any) {
  const [inputValue, setInputValue] = React.useState("");
  const [values, setValues] = React.useState<object[]>([]);
  const errors = props.errors;
  const touched = props.touched;
  const { t } = useTranslation();
  const label: string = t(props.label);
  const formik: FormikContextType<FormikValues> = props.formik;
  const { getQuery } = AxiosConnection();
  const inputValuePrev = usePrevious(inputValue);

  const debounceValues = useRef(
    debounce(
      (breedInputValue: string, formik: any, url: string) => {
        getQuery(url + breedInputValue)
          .then((response: any) => {
            const data: object[] = values;
            response.data["hydra:member"].forEach((item: any) => {
              data[item.id] = {
                "@id": item["@id"],
                id: item.id,
                name: item.name,
              };
            });
            setValues(data);
            formik.setFieldValue(props.name, breedInputValue, false);
          })
          .catch(() => {
            toast.error("Impossible de récupérer les races de chiens", {
              position: "bottom-right",
              autoClose: 3000,
              hideProgressBar: true,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true,
              toastId: "get-breeds-error",
            });
          });
      },
      500,
      { trailing: true }
    )
  ).current;

  const getValues = useCallback(() => {
    if (inputValue === inputValuePrev) {
      return false;
    }
    debounceValues(inputValue, formik, props.url);
  }, [inputValue, debounceValues, inputValuePrev, formik, props.url]);

  useEffect(() => {
    getValues();
  }, [inputValue, getValues]);
  let allowedPropsTextField = pick(props, [
    "id",
    "type",
    "label",
    "name",
    "value",
    "placeholder",
    "required",
    "error",
  ]);
  return (
    <MuiAutocomplete
      disabled={props.formik.isSubmitting}
      freeSolo={true}
      autoComplete={true}
      autoHighlight={true}
      {...allowedPropsTextField}
      renderInput={(params: any) => {
        return (
          <TextField
            label={label}
            {...params}
            value={props.value}
            required={props.required}
            error={touched && Boolean(errors)}
            helperText={touched && errors}
          />
        );
      }}
      getOptionLabel={(option: any) => {
        return typeof option === "string" ? option : option?.name ?? "";
      }}
      isOptionEqualToValue={(option: any, value: any) => option.id === value.id}
      onChange={(event: any, value: any) => {
        console.log("onChange", event, value);
        formik.setFieldValue(props.name, value);
      }}
      onInputChange={(event: React.SyntheticEvent, newInputValue: any) => {
        if (inputValue === newInputValue) {
          return false;
        }
        if (formik?.status) {
          formik.setStatus(undefined);
        }
        // if (event?.type === "change" || (!event && !!newInputValue)) {
        setInputValue(newInputValue);
        formik.setFieldValue(props.name, newInputValue, true);
        // }
      }}
      options={values}
    />
  );
}

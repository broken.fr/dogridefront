import { pick } from "lodash";
import React from "react";
import { Button as MuiButton, CircularProgress } from "@mui/material";
import { green } from "@mui/material/colors";
import Grid from "@mui/material/Grid";

export default function Button(props: any) {
  let allowedPropsTextField = pick(props, [
    "id",
    "type",
    "label",
    "name",
    "value",
    "onChange",
    "onBlur",
    "onFocus",
    "placeholder",
    "required",
    "children",
    "endIcon",
    "startIcon",
    "variant",
  ]);
  return (
    <Grid container justifyContent="end">
      <Grid item xs={6} sm={6} md={4} lg={2} sx={{ position: "relative" }}>
        <MuiButton
          {...allowedPropsTextField}
          fullWidth
          disabled={props.formik.isSubmitting}
        />
        {props.formik.isSubmitting && (
          <CircularProgress
            size={24}
            sx={{
              color: green[500],
              position: "absolute",
              top: "50%",
              left: "50%",
              marginTop: "-12px",
              marginLeft: "-12px",
            }}
          />
        )}
      </Grid>
    </Grid>
  );
}

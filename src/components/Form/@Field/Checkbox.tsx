import { pick } from "lodash";
import React from "react";
import {
  Checkbox as MuiCheckbox,
  FormControlLabel,
  FormHelperText, Typography, useTheme,
} from "@mui/material";
import { useTranslation } from "react-i18next";
import Grid from "@mui/material/Grid";
import { red } from "@mui/material/colors";
import {Link} from "react-router-dom";
export default function Checkbox(props: any) {
  const theme = useTheme();
  function linkRide(label: string) {
    if(label === "walk.rules_acceptance"){
      return (<Typography component={Link} to="/footer/RecommendationsWalk" color={theme.palette.text.primary}>Préconisations pour une Balade</Typography>)
    }
  }
  const { t } = useTranslation();
  const label = t(props.label);

  // props.formik.setFieldValue(props.name, Boolean(props.value));
  // props.value = props.value ? true : false;
  let allowedPropsTextField = pick(props, [
    "id",
    "type",
    "onChange",
    "onBlur",
    "onFocus",
    "placeholder",
    "required",
    "value",
    "link",
  ]);
  return (
    <Grid
      container
      direction={"column"}
      justifyContent={"center"}
      alignItems={"center"}
    >
      <Grid item xs={12}>
        {linkRide(props.label)}
        <FormControlLabel
          {...allowedPropsTextField}
          disabled={props.formik.isSubmitting}
          checked={props.value === "false" ? false : Boolean(props.value)}
          sx={
            props.errors
              ? {
                  color: red[800],
                }
              : {}
          }
          control={
            <MuiCheckbox
              checked={props.value === "false" ? false : Boolean(props.value)}
              required={props.required}
              sx={
                props.errors
                  ? {
                      color: red[800],
                    }
                  : {}
              }
              name={props.name}
              value={props.value === "false" ? false : props.value}
            />
          }
          label={label}
        />

      </Grid>

      {props.errors &&
        props.errors.map((error: string, index: number) => {
          return (
            <Grid key={index} item xs={10}>
              <FormHelperText key={index} error={true}>
                {error}
              </FormHelperText>
            </Grid>
          );
        })}
    </Grid>
  );
}

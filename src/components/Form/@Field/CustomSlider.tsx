import {FieldProps} from 'formik';
import Slider, { SliderProps as MuiSliderProps} from '@mui/material/Slider';
import React from "react";
import {Typography} from "@mui/material";
type Omit<T, K extends keyof T> = Pick<T, Exclude<keyof T, K>>;
export interface SliderProps
    extends FieldProps,
        Omit<MuiSliderProps, 'name' | 'onChange' | 'value' | 'defaultValue'> {
    label?: string
}

export function fieldToSlider({
                                  field,
                                  disabled = false,
                                  ...props
                              }: SliderProps): MuiSliderProps {
    return {
        disabled: disabled,
        ...props,
        ...field,
        name: field.name,
        value: field.value,
    };
}

export const CustomSlider: React.ComponentType<SliderProps> = (props: SliderProps) => (
    <>
        <Typography id="track-false-slider" gutterBottom>
            {props.label}
        </Typography>
        <Slider
            {...fieldToSlider(props)}
            onChange={(e, value) => {
                props.form.setFieldValue(props.field.name, value);
            }}
        />
    </>
);
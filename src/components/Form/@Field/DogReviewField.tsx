import React from "react";
import { Avatar, Grid, Typography } from "@mui/material";
import moment from "moment/moment";
import Config from "../../../config.json";
import { MediaObject } from "../../../interfaces/mediaobject";
import PetsIcon from "@mui/icons-material/Pets";
import GenderIcon from "@mui/icons-material/EmojiPeople";
import BloodtypeIcon from "@mui/icons-material/Bloodtype";
import { Breed } from "../../../interfaces/breed";
import CalendarTodayIcon from "@mui/icons-material/CalendarToday";
import { styled } from "@mui/material/styles";
import { Cookie, CookieOutlined } from "@mui/icons-material";
import ReviewField from "./ReviewField";

const StyledReview = styled(ReviewField)({
  "& .MuiRating-iconFilled": {
    color: "#917070",
  },
  "& .MuiRating-iconHover": {
    color: "#91785b",
  },
});

export default function DogReviewField(props: any) {
  const getAgeString = (birthdate: moment.Moment): string => {
    const ageYears = moment().diff(birthdate, "years");
    const ageMonths = moment().diff(birthdate, "months") % 12;
    return ageYears > 0
      ? `${ageYears} an${ageYears > 1 ? "s" : ""}`
      : `${ageMonths} mois`;
  };
  const dog = props.dog;
  const birthdate = moment(dog.birthdate);
  const ageString = getAgeString(birthdate);
  return (
    <Grid container sx={{ my: 3 }} alignItems="center" spacing={2}>
      <Grid item xs={3} justifyContent="center">
        <Grid container spacing={2} alignItems="center" justifyContent="center">
          <Grid item>
            <Avatar
              variant="circular"
              src={
                Config.REACT_APP_DOGGOBOX_URL +
                (dog?.image as MediaObject)?.content_url
              }
              sx={{ width: 150, height: 150 }}
            />
          </Grid>
        </Grid>
      </Grid>

      <Grid item xs={6}>
        <Grid container spacing={2} alignItems="center">
          <Grid item>
            <PetsIcon sx={{ fontSize: "2.5rem", color: "#6b7280" }} />
          </Grid>
          <Grid item>
            <Typography variant="subtitle2" sx={{ fontWeight: "bold" }}>
              {dog?.name}
            </Typography>
          </Grid>
        </Grid>

        <Grid container spacing={2} alignItems="center">
          <Grid item>
            <GenderIcon sx={{ fontSize: "2.5rem", color: "#6b7280" }} />
          </Grid>
          <Grid item>
            <Typography variant="subtitle2" sx={{ fontWeight: "bold" }}>
              {dog?.gender === "female" ? "Femelle" : "Male"}
            </Typography>
          </Grid>
        </Grid>

        <Grid container spacing={2} alignItems="center">
          <Grid item>
            <BloodtypeIcon sx={{ fontSize: "2.5rem", color: "#6b7280" }} />
          </Grid>
          <Grid item>
            <Typography variant="subtitle2" sx={{ fontWeight: "bold" }}>
              {(dog?.breed as Breed)?.name}
            </Typography>
          </Grid>
        </Grid>

        <Grid container spacing={2} alignItems="center">
          <Grid item>
            <CalendarTodayIcon sx={{ fontSize: "2.5rem", color: "#6b7280" }} />
          </Grid>
          <Grid item>
            <Typography variant="subtitle2" sx={{ fontWeight: "bold" }}>
              {ageString}
            </Typography>
          </Grid>
        </Grid>
      </Grid>
      <Grid item xs={3}>
        <StyledReview
          {...props}
          icon={<Cookie fontSize="inherit" />}
          emptyIcon={<CookieOutlined fontSize="inherit" />}
        />
      </Grid>
    </Grid>
  );
}

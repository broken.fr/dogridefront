import { omit } from "lodash";
import React from "react";
import Grid from "@mui/material/Grid";
import { FormControl, Typography } from "@mui/material";
import { useTranslation } from "react-i18next";

export default function FieldWithTitle(props: any) {
  const { t } = useTranslation();
  const Field = props.field;
  const title = props.title;
  const size = Boolean(props.multiline)
    ? { xs: 10, sm: 10, md: 10, lg: 10 }
    : { xs: 10, sm: 10, md: 6, lg: 4 };
  let allowedPropsTextField = omit(props, ["field"]);
  return (
    <>
      <Grid item xs={12} sm={12} md={12} lg={12}>
        <Typography variant={"h3"} fontSize={"20px"} mt={"60px"} gutterBottom>
          {t(title)}
        </Typography>
      </Grid>
      <Grid item {...size}>
        <FormControl fullWidth>
          <Field {...allowedPropsTextField} />
        </FormControl>
      </Grid>
    </>
  );
}

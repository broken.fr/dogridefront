import { pick } from "lodash";
import React from "react";
import { Avatar, FormControl, Typography } from "@mui/material";
import { FormikValues } from "formik";
import { FormikContextType } from "formik/dist/types";
import Button from "@mui/material/Button";
import { PhotoCamera } from "@mui/icons-material";
import { useTranslation } from "react-i18next";
import Grid from "@mui/material/Grid";
import Config from "../../../config.json";

export default function FileInput(props: any) {
  const errors = props.errors;
  let allowedPropsTextField = pick(props, [
    "id",
    "label",
    "name",
    // "onChange",
    // "onBlur",
    // "onFocus",
    "placeholder",
    "required",
  ]);
  const formik: FormikContextType<FormikValues> = props.formik;
  const { t } = useTranslation();
  return (
    <Grid
      container
      direction="row"
      justifyContent="center"
      alignItems="center"
      spacing={4}
    >
      <Grid item xs={12} sm={12} md={6} lg={6}>
        <Avatar
          src={
            typeof props.value === "object"
              ? Config.REACT_APP_DOGGOBOX_URL + props.value.content_url
              : props.value
          }
          variant={"rounded"}
          sx={{ width: "100%", height: "100%", marginBottom: "10px" }}
        />
      </Grid>
      <Grid item xs={12} sm={12} md={6} lg={6}>
        <FormControl fullWidth>
          <Button
            color={errors ? "error" : "primary"}
            variant="outlined"
            sx={{ py: "14px" }}
            component="label"
            size="large"
            startIcon={<PhotoCamera />}
            disabled={props.formik.isSubmitting}
          >
            <input
              type="file"
              accept="image/*"
              hidden
              {...allowedPropsTextField}
              disabled={props.formik.isSubmitting}
              onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
                const file = event.target.files ? event.target.files[0] : null;
                const fileReader = new FileReader();
                fileReader.onload = () => {
                  if (fileReader.readyState === 2) {
                    formik.setFieldValue(props.name, fileReader.result);
                  }
                };
                if (file) {
                  fileReader.readAsDataURL(file);
                }
              }}
            />
            {t(props.label)}
          </Button>

          {errors && (
            <Typography variant="subtitle2" sx={{ color: "red" }}>
              {errors}
            </Typography>
          )}
        </FormControl>
      </Grid>
    </Grid>
  );
}

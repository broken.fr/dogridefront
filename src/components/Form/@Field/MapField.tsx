import { pick } from "lodash";
import React, { useCallback, useEffect, useState } from "react";
import { FormControl, FormHelperText, Input, Typography } from "@mui/material";
import { MapContainer, Marker, TileLayer, useMapEvents } from "react-leaflet";
import L from "leaflet";
import "leaflet/dist/leaflet.css";
import Grid from "@mui/material/Grid";

let DefaultIcon = L.icon({
  iconUrl: require("leaflet/dist/images/marker-icon.png"),
  shadowUrl: require("leaflet/dist/images/marker-shadow.png"),
  iconSize: [24, 40],
  iconAnchor: [12, 38],
});

L.Marker.prototype.options.icon = DefaultIcon;

interface MarkersProps {
  initialPosition: [number, number];
  setInitialPosition: React.Dispatch<React.SetStateAction<[number, number]>>;
  defaultPos: [number, number];
  isLocated: boolean;
  setIsLocated: React.Dispatch<React.SetStateAction<boolean>>;
  props: any;
}

const Markers: React.FC<MarkersProps> = ({
  initialPosition,
  setInitialPosition,
  defaultPos,
  isLocated,
  setIsLocated,
  props,
}) => {
  const map = useMapEvents({
    click(e) {
      if (!props.formik.isSubmitting) {
        props.formik.setFieldValue(props.name, [e.latlng.lat, e.latlng.lng]);
      }
    },
    locationfound(e) {
      setInitialPosition([e.latlng.lat, e.latlng.lng]);
      map.flyTo(e.latlng, map.getZoom());
    },
  });

  const locate = useCallback(() => {
    map.locate();
    setIsLocated(true);
  }, [map, setIsLocated]);

  useEffect(() => {
    if (initialPosition === defaultPos && !props.value && !isLocated) {
      locate();
    }
  }, [defaultPos, initialPosition, isLocated, locate, props.value]);

  return props.value !== "" ? (
    <Marker key={props.value[0]} position={props.value} interactive={false} />
  ) : null;
};
export default function MapField(props: any) {
  const defaultPos = props.value !== "" ? props.value : [48.8539418, 2.3298689];
  const [initialPosition, setInitialPosition] = useState<[number, number]>(
    defaultPos as [number, number]
  );
  const [isLocated, setIsLocated] = useState(false);

  const errors = props.errors;
  let allowedPropsTextField = pick(props, [
    "id",
    "type",
    "label",
    "name",
    "value",
    "onChange",
    "onBlur",
    "onFocus",
    "placeholder",
    "required",
    "rows",
    "multiline",
  ]);
  return (
    <>
      <Grid
        key={"map"}
        item
        lg={12}
        md={12}
        sm={12}
        xs={12}
        marginBottom={4}
        // paddingLeft={2}
        // marginRight={3}
      >
        <FormControl fullWidth={true} error={Boolean(errors)}>
          <Grid
            container
            direction={"column"}
            justifyContent={"center"}
            alignItems={"center"}
          >
            <Grid item>
              <Typography variant={"caption"}>
                Veuillez sélectionner le point de départ de la balade en
                cliquant sur la carte ci-dessous.
              </Typography>
              <MapContainer
                center={initialPosition}
                zoom={13}
                style={{ height: "50vh", width: "100vs", maxHeight: "500px" }}
              >
                <TileLayer
                  attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                  url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                />
                <Markers
                  initialPosition={initialPosition}
                  setInitialPosition={setInitialPosition}
                  defaultPos={defaultPos}
                  isLocated={isLocated}
                  setIsLocated={setIsLocated}
                  props={props}
                />
              </MapContainer>
            </Grid>
            <Grid item>
              <Input {...allowedPropsTextField} type={"hidden"} />
              {errors &&
                errors.map((error: string, index: number) => {
                  return (
                    <FormHelperText key={index} error={true}>
                      {error}
                    </FormHelperText>
                  );
                })}
            </Grid>
          </Grid>
        </FormControl>
      </Grid>
    </>
  );
}

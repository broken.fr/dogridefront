import { pick } from "lodash";
import React from "react";
import { useTranslation } from "react-i18next";
import { MobileDateTimePicker as MuiMobileDateTimePicker } from "@mui/x-date-pickers";
import { FormikContextType } from "formik/dist/types";
import { FormikValues } from "formik";

export default function MobileDateTimePicker(props: any) {
  const errors = props.errors;
  const { t } = useTranslation();
  const label = t(props.label);
  const formik: FormikContextType<FormikValues> = props.formik;
  let allowedPropsTextField = pick(props, [
    "id",
    "type",
    "label",
    "name",
    "value",
    "onBlur",
    "onFocus",
    "placeholder",
    "required",
    "disablePast",
  ]);
  return (
    <MuiMobileDateTimePicker
      {...allowedPropsTextField}
      label={label}
      disabled={props.formik.isSubmitting}
      slotProps={{
        textField: {
          helperText: errors,
          error: Boolean(errors),
        },
      }}
      onChange={(value: any) => {
        return formik.setFieldValue(props.name, value, true);
      }}
    />
  );
}

import React from "react";
import { FormHelperText, Grid } from "@mui/material";
import Rating from "@mui/material/Rating";
import { pick } from "lodash";
import { FormikProps } from "formik";

export default function ReviewField(props: any) {
  const errors = props.errors;
  const formik: FormikProps<any> = props.formik;
  let allowedPropsTextField = pick(props, [
    "id",
    "type",
    "label",
    "name",
    "icon",
    "emptyIcon",
    "className",
  ]);

  return (
    <Grid container direction={"column"} alignContent={"center"}>
      <Grid item>
        <Rating
          precision={1}
          defaultValue={0}
          {...allowedPropsTextField}
          value={Number(props.value) ?? 0}
          onChange={(e, value) => {
            formik.setFieldValue(props.name, Number(value));
          }}
        />
      </Grid>
      {errors &&
        errors.map((error: string, index: number) => {
          return (
            <Grid key={index} item>
              <FormHelperText error={true}>{error}</FormHelperText>
            </Grid>
          );
        })}
    </Grid>
  );
}

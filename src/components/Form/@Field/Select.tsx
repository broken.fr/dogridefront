import { pick } from "lodash";
import React from "react";
import {
  Box,
  Checkbox,
  Chip,
  FormHelperText,
  InputLabel,
  MenuItem,
  Select as MuiSelect,
  SelectChangeEvent, Typography,
} from "@mui/material";
import { FormikContextType } from "formik/dist/types";
import { FormikValues } from "formik";

import i18next from "i18next";
import IconButton from "@mui/material/IconButton";
import HelpIcon from "@mui/icons-material/Help";
import Popover from "@mui/material/Popover";
import {useTranslation} from "react-i18next";

export default function Select(props: any) {
  const label: string = i18next.t(props.label);
  const formik: FormikContextType<FormikValues> = props.formik;
  const errors = props.errors;
    const { t } = useTranslation();


    function popover(help: string) {
        // eslint-disable-next-line react-hooks/rules-of-hooks
        const [anchorEl, setAnchorEl] = React.useState<HTMLButtonElement | null>(null);

        const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
            setAnchorEl(event.currentTarget);
        };

        const handleClose = () => {
            setAnchorEl(null);
        };

        const open = Boolean(anchorEl);
        const id = open ? 'simple-popover' : undefined;

        return (
            <>
                <IconButton aria-describedby={id} onClick={handleClick}>
                    <HelpIcon color="primary"/>
                </IconButton>
                <Popover
                    id={id}
                    open={open}
                    anchorEl={anchorEl}
                    onClose={handleClose}
                    anchorOrigin={{
                        vertical: 'center',
                        horizontal: 'right',
                    }}
                    transformOrigin={{
                        vertical: 'center',
                        horizontal: 'left',
                    }}
                >
                    <Typography sx={{ p: 2 }}>{help}</Typography>
                </Popover>
            </>
        );
    }

  let allowedPropsTextField = pick(props, [
    // "id",
    // "type",
    "name",
    "formik",
    "renderValue",
    "multiple",
    "value",
    // "onChange",
    "onBlur",
    "onFocus",
    "placeholder",
    "required",
    // "error",
  ]);

  return (
    <>
      <InputLabel id={props.name}>{label}</InputLabel>

      <MuiSelect
        label={label}
        error={Boolean(errors)}
        disabled={props.formik.isSubmitting}
        {...(props.multiple && {
          renderValue: (value: any) => {
            if (value.constructor === Array) {
              const label: any[] = [];
              value.map((value) => {
                label.push(props.menuItems?.find((val: any) => val === value));
                return null;
              });
              return (
                <Box
                  sx={{
                    display: "flex",
                    flexWrap: "wrap",
                    gap: 0.5,
                  }}
                >
                  {label.map((value) => (
                    <Chip key={value.dog} label={value.label} />
                  ))}
                </Box>
              );
            }
          },
        })}
        onChange={(event: SelectChangeEvent) => {
          formik.setFieldValue(props.name, event.target?.value, true);
        }}
        {...allowedPropsTextField}
      >
        {props.menuItems?.map((menuItem: any, index: number) => {
          return (
            <MenuItem
              key={index}
              id={props.multiple ? menuItem.dog : menuItem.value + "_" + index}
              value={props.multiple ? menuItem : menuItem.value}
            >
              {props?.multiple ? (
                <Checkbox
                  id={menuItem.dog + "_" + index + "_checkbox"}
                  checked={formik.values[props.name].indexOf(menuItem) > -1}
                />
              ) : null}
              {i18next.t(menuItem.label)}
            </MenuItem>
          );
        })}
      </MuiSelect>{props?.help ? popover(t(props.help)): null}
      {errors &&
        errors.map((error: string, index: number) => {
          return (
            <FormHelperText key={index} error={true}>
              {error}
            </FormHelperText>
          );
        })}
    </>
  );
}

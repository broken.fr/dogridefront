import { pick } from "lodash";
import React from "react";
import { useTranslation } from "react-i18next";
import { FormHelperText, Slider as MuiSlider, Typography } from "@mui/material";
import { FormikContextType } from "formik/dist/types";
import { FormikValues } from "formik";

import Popover from '@mui/material/Popover';
import IconButton from '@mui/material/IconButton';
import Button from '@mui/material/Button';
import HelpIcon from '@mui/icons-material/Help';

export default function Slider(props: any) {
  function popover(help: string) {
    // eslint-disable-next-line react-hooks/rules-of-hooks
    const [anchorEl, setAnchorEl] = React.useState<HTMLButtonElement | null>(null);

    const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
      setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
      setAnchorEl(null);
    };

    const open = Boolean(anchorEl);
    const id = open ? 'simple-popover' : undefined;

    return (
        <>
          <IconButton aria-describedby={id} onClick={handleClick}>
            <HelpIcon color="primary"/>
          </IconButton>
          <Popover
              id={id}
              open={open}
              anchorEl={anchorEl}
              onClose={handleClose}
              anchorOrigin={{
                vertical: 'center',
                horizontal: 'right',
              }}
              transformOrigin={{
                vertical: 'center',
                horizontal: 'left',
              }}
          >
            <Typography sx={{ p: 2 }}>{help}</Typography>
          </Popover>
        </>
    );
  }
  const errors = props.errors;
  const { t } = useTranslation();
  const label = t(props.label);
  const formik: FormikContextType<FormikValues> = props.formik;
  let allowedPropsTextField = pick(props, [
    "id",
    "type",
    "name",
    "onChange",
    "onBlur",
    "onFocus",
    "placeholder",
    "required",
    "min",
    "max",
    "step",
    "marks",
    "valueLabelDisplay",
    "value"
  ]);
  return (
    <>
      <Typography  gutterBottom>{label} {props?.help ? popover(t(props.help)): null}</Typography>

      <MuiSlider
        {...allowedPropsTextField}
        disabled={props.formik.isSubmitting}
        onChange={(e, value) => {
          formik.setFieldValue(props.name, value);
        }}
      />
      {errors &&
        errors.map((error: string, index: number) => {
          return (
            <FormHelperText key={index} error={true}>
              {error}
            </FormHelperText>
          );
        })}
    </>
  );
}

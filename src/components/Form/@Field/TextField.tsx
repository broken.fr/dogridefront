import { pick } from "lodash";
import React from "react";
import { TextField as MuiTextField } from "@mui/material";
import { useTranslation } from "react-i18next";

export default function TextField(props: any) {
  const errors = props.errors;
  const touched = props.touched;
  const { t } = useTranslation();
  const label = t(props.label);
  let allowedPropsTextField = pick(props, [
    "id",
    "type",
    "label",
    "name",
    "value",
    "onChange",
    "onBlur",
    "onFocus",
    "placeholder",
    "required",
    "rows",
    "multiline",
  ]);
  return (
    <MuiTextField
      disabled={props.formik.isSubmitting}
      {...allowedPropsTextField}
      label={label}
      fullWidth
      error={touched && Boolean(errors)}
      helperText={touched && errors}
    />
  );
}

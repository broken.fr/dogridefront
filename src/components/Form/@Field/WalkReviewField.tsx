import React from "react";
import { Card, CardContent, CardHeader, Grid, Typography } from "@mui/material";
import moment from "moment/moment";
import CalendarTodayIcon from "@mui/icons-material/CalendarToday";
import { Walk } from "../../../interfaces/walk";
import QueryBuilderIcon from "@mui/icons-material/QueryBuilder";
import PeopleIcon from "@mui/icons-material/People";
import { useTranslation } from "react-i18next";
import Skeleton from "@mui/material/Skeleton";
import ReviewField from "./ReviewField";

export default function WalkReviewField(props: any) {
  const walk: Walk = props.walk;
  const { t } = useTranslation();
  let numberOfAcceptedParticipants;
  let durationString;
  if (walk) {
    numberOfAcceptedParticipants = walk.walk_subscriptions?.reduce(
      (acceptedTotal, walk_subscriptions) =>
        walk_subscriptions.accepted === true ? ++acceptedTotal : acceptedTotal,
      0
    );
    const duration = moment.duration(walk.walk_duration, "minutes");
    const hours = Math.floor(duration.asHours());
    const minutes = duration.minutes();
    durationString =
      hours > 0
        ? `${hours}h${minutes.toString().padStart(2, "0")}`
        : `${minutes}min`;
  } else {
    numberOfAcceptedParticipants = (
      <Skeleton width={30} sx={{ display: "inline-block" }} />
    );
    durationString = <Skeleton sx={{ width: "100%" }} />;
  }
  return (
    <Card>
      <CardHeader
        title={
          <Typography variant={"h5"}>
            {t("review.walk.review")}
            {walk ? walk.description : <Skeleton />}
          </Typography>
        }
      />
      <CardContent>
        <Grid container alignItems="center" spacing={2}>
          <Grid item xs={6} sm={3}>
            <Typography variant="subtitle1" color="textSecondary">
              {t("review.walk.date")}
            </Typography>
            <div style={{ display: "flex", alignItems: "center" }}>
              <CalendarTodayIcon
                color="primary"
                style={{ marginRight: "8px" }}
              />
              <Typography variant="body1" component="p" sx={{ width: "100%" }}>
                {walk ? (
                  moment(walk.walk_date as string).format("llll")
                ) : (
                  <Skeleton sx={{ width: "100%" }} />
                )}
              </Typography>
            </div>
          </Grid>
          <Grid item xs={6} sm={3}>
            <Typography variant="subtitle1" color="textSecondary">
              {t("review.walk.duration")}
            </Typography>
            <div style={{ display: "flex", alignItems: "center" }}>
              <QueryBuilderIcon
                color="primary"
                style={{ marginRight: "8px" }}
              />
              <Typography variant="body1" sx={{ width: "100%" }} component="p">
                {durationString}
              </Typography>
            </div>
          </Grid>
          <Grid item xs={6} sm={3}>
            <Typography variant="subtitle1" color="textSecondary">
              {t("review.walk.participants")}
            </Typography>
            <div style={{ display: "flex", alignItems: "center" }}>
              <PeopleIcon color="primary" style={{ marginRight: "8px" }} />
              <Typography variant="body1" component="p">
                {numberOfAcceptedParticipants} / &nbsp;
                {walk ? (
                  walk.participants_max
                ) : (
                  <Skeleton width={30} sx={{ display: "inline-block" }} />
                )}
              </Typography>
            </div>
          </Grid>
          <Grid item xs={6} sm={3}>
            <ReviewField {...props} />
          </Grid>
        </Grid>
      </CardContent>
    </Card>
  );
}

import FormBuilder from "@elevenlabs/react-formbuilder";
import theme from "./theme";
import translate from "./translate";

const LoginForm = () => {
  // @ts-ignore
  const form = new FormBuilder()
    .setTheme(theme)
    .setTranslate(translate)
    .createForm()
    .add("email", "TextField", {
      label: "Email",
      required: true,
      validators: ["Required", "Email"],
      initialValue: "john@gmail.com",
    })
    .add("password", "TextField", {
      type: "password",
      label: "Password",
      required: true,
      validators: ["Required", ["IsGreaterThan", { length: 6 }]],
    })
    .add("image", "FileInput", {
      label: "Image",
      required: true,
    })
    .add("submit", "Button", {
      type: "submit",
      children: "Login",
    });

  return form;
};

export default LoginForm;

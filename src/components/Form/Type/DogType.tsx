import theme from "../theme";
import Translator from "../translate";
import FormBuilder from "@elevenlabs/react-formbuilder";
import TextField from "../@Field/TextField";
import Slider from "../@Field/Slider";
import Checkbox from "../@Field/Checkbox";
import { Dog } from "../../../interfaces/dog";
import { MediaObject } from "../../../interfaces/mediaobject";
import Config from "../../../config.json";
import FileInput from "../@Field/FileInput";
import SendIcon from "@mui/icons-material/Send";
import i18next from "i18next";
import React from "react";

const DogType = (dog?: Dog) => {
  const formBuilder = new FormBuilder()
    .setTheme(theme)
    .setTranslate(Translator)
    .createForm()
    .add("image", "FieldWithTitle", {
      field: FileInput,
      multiline: true,
      label: "Image",
      validators: ["Required"],
      initialValue: (dog?.image as MediaObject)?.content_url
        ? Config.REACT_APP_DOGGOBOX_URL +
          (dog?.image as MediaObject)?.content_url
        : undefined,
    })
    .add("name", "TextField", {
      label: "dog.name",
      required: true,
      validators: ["Required", ["IsGreaterThan", { length: 3 }]],
      initialValue: dog?.name,
    })
    .add("breed", "Autocomplete", {
      label: "dog.breed",
      required: true,
      validators: ["Required"],
      url: "breeds?name=",
      initialValue: dog?.breed,
    })
    .add("sterilized", "Select", {
      label: "dog.sterilized.label",
      validators: [
        [(v: any) => !!v || v === false, "form.validators.required"],
      ],
      menuItems: [
        { value: true, label: "dog.sterilized.yes" },
        { value: false, label: "dog.sterilized.no" },
      ],
      initialValue: dog?.sterilized,
    })
    .add("gender", "Select", {
      label: "dog.gender",
      required: true,
      validators: ["Required"],
      menuItems: [
        { value: "male", label: "dog.gender_types.male" },
        { value: "female", label: "dog.gender_types.female" },
      ],
      initialValue: dog?.gender,
    })
    .add("category", "Select", {
      label: "dog.category.label",
      help: "dog.category.help",
      required: true,
      validators: ["Required"],
      menuItems: [
        { value: "category_1", label: "dog.category.category_1" },
        { value: "category_2", label: "dog.category.category_2" },
        { value: "category_3", label: "dog.category.category_3" },
      ],
      initialValue: dog?.category,
    })
    .add("birthdate", "MobileDatePicker", {
      label: "dog.birthdate",
      validators: ["Required"],
      disableFuture: true,
      initialValue: dog?.birthdate,
    })
    .add("amity_males", "FieldWithTitle", {
      field: Slider,
      label: "dog.amity_males",
      min: 0,
      max: 5,
      step: 1,
      marks: true,
      valueLabelDisplay: "auto",
      initialValue: dog?.amity_males ?? 2,
      title: "dog.amity_males_title",
    })
    .add("amity_females", "Slider", {
      label: "dog.amity_females",
      min: 0,
      max: 5,
      step: 1,
      marks: true,
      valueLabelDisplay: "auto",
      initialValue: dog?.amity_females ?? 2,
    })
    .add("amity_puppies", "Slider", {
      label: "dog.amity_puppies",
      min: 0,
      max: 5,
      step: 1,
      marks: true,
      valueLabelDisplay: "auto",
      initialValue: dog?.amity_puppies ?? 2,
    })
    .add("playfullness", "Slider", {
      label: "dog.playfullness",
      min: 0,
      max: 5,
      step: 1,
      marks: true,
      valueLabelDisplay: "auto",
      initialValue: dog?.playfullness ?? 2,
    })
    .add("runner", "Slider", {
      label: "dog.runner",
      min: 0,
      max: 5,
      step: 1,
      marks: true,
      valueLabelDisplay: "auto",
      initialValue: dog?.runner ?? 2,
    })
    .add("sharing", "Slider", {
      label: "dog.sharing",
      min: 0,
      max: 5,
      step: 1,
      marks: true,
      valueLabelDisplay: "auto",
      initialValue: dog?.sharing ?? 2,
    })
    .add("sniffer", "Slider", {
      label: "dog.sniffer",
      min: 0,
      max: 5,
      step: 1,
      marks: true,
      valueLabelDisplay: "auto",
      initialValue: dog?.sniffer ?? 2,
    })
    .add("swimmer", "Slider", {
      label: "dog.swimmer",
      min: 0,
      max: 5,
      step: 1,
      marks: true,
      valueLabelDisplay: "auto",
      initialValue: dog?.swimmer ?? 2,
    })
    .add("fighter", "Slider", {
      label: "dog.fighter",
      min: 0,
      max: 5,
      step: 1,
      marks: true,
      valueLabelDisplay: "auto",
      initialValue: dog?.fighter ?? 2,
    })
    .add("adventurous_temper", "FieldWithTitle", {
      field: Checkbox,
      label: "dog.adventurous_temper",
      initialValue: dog?.adventurous_temper ?? false,
      required: false,
      title: "dog.adventurous_temper_title",
    })
    .add("grumpy_temper", "Checkbox", {
      label: "dog.grumpy_temper",
      initialValue: dog?.grumpy_temper ?? false,
      required: false,
    })
    .add("peaceful_temper", "Checkbox", {
      label: "dog.peaceful_temper",
      initialValue: dog?.peaceful_temper ?? false,
      required: false,
    })
    .add("brawler_temper", "Checkbox", {
      label: "dog.brawler_temper",
      initialValue: dog?.brawler_temper ?? false,
      required: false,
    })
    .add("independent_temper", "Checkbox", {
      label: "dog.independent_temper",
      initialValue: dog?.independent_temper ?? false,
      required: false,
    })
    .add("brutal_temper", "Checkbox", {
      label: "dog.brutal_temper",
      initialValue: dog?.brutal_temper ?? false,
      required: false,
    })
    .add("mild_temper", "Checkbox", {
      label: "dog.mild_temper",
      initialValue: dog?.mild_temper ?? Boolean(false),
      required: false,
    })
    .add("shy_temper", "Checkbox", {
      label: "dog.shy_temper",
      initialValue: dog?.shy_temper ?? false,
      required: false,
    })
    .add("stop_response", "FieldWithTitle", {
      field: Slider,
      label: "dog.stop_response",
      min: 0,
      max: 5,
      step: 1,
      marks: true,
      valueLabelDisplay: "auto",
      initialValue: dog?.stop_response ?? 2,
      title: "dog.stop_response_title",
    })
    .add("return_response", "Slider", {
      label: "dog.return_response",
      min: 0,
      max: 5,
      step: 1,
      marks: true,
      valueLabelDisplay: "auto",
      initialValue: dog?.return_response ?? 2,
    })
    .add("energy_level", "FieldWithTitle", {
      field: Slider,
      title: "dog.energy_level_title",
      label: "dog.energy_level",
      min: 0,
      max: 5,
      step: 1,
      marks: true,
      valueLabelDisplay: "auto",
      initialValue: dog?.energy_level ?? 2,
    })
    .add("resume", "FieldWithTitle", {
      field: TextField,
      title: "dog.resume_title",
      label: "dog.resume",
      initialValue: dog?.resume,
      multiline: true,
      rows: 4,
      required: false,
    })
    .add("submit", "Button", {
      type: "submit",
      variant: "contained",
      endIcon: <SendIcon />,
      children: dog ? i18next.t("save") : i18next.t("create"),
    });

  formBuilder._initialValues = {
    adventurous_temper: false,
    amity_females: dog?.amity_females ?? 2,
    amity_males: dog?.amity_males ?? 2,
    amity_puppies: dog?.amity_puppies ?? 2,
    birthdate: dog?.birthdate ?? "",
    brawler_temper: dog?.brawler_temper ?? false,
    breed: dog?.breed ?? "",
    brutal_temper: dog?.brutal_temper ?? false,
    category: dog?.category ?? "",
    energy_level: dog?.energy_level ?? 2,
    fighter: dog?.fighter ?? 2,
    gender: dog?.gender ?? "",
    grumpy_temper: dog?.grumpy_temper ?? false,
    image: dog?.image ?? "",
    independent_temper: dog?.independent_temper ?? false,
    mild_temper: dog?.mild_temper ?? false,
    name: dog?.name ?? "",
    peaceful_temper: dog?.peaceful_temper ?? false,
    playfullness: dog?.playfullness ?? 2,
    resume: dog?.resume ?? "",
    return_response: dog?.return_response ?? 2,
    runner: dog?.runner ?? 2,
    sharing: dog?.sharing ?? 2,
    shy_temper: dog?.shy_temper ?? false,
    sniffer: dog?.sniffer ?? 2,
    sterilized: dog?.sterilized ?? "",
    stop_response: dog?.stop_response ?? 2,
    swimmer: dog?.swimmer ?? 2,
  };
  return formBuilder;
};

export default DogType;

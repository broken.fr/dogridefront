import theme from "../theme";
import Translator from "../translate";
import FormBuilder from "@elevenlabs/react-formbuilder";
import React from "react";
import { Walk } from "../../../interfaces/walk";
import SendIcon from "@mui/icons-material/Send";
import i18next from "i18next";
import { Dog } from "../../../interfaces/dog";
import { WalkSubscription } from "../../../interfaces/walksubscription";
import { isNumber } from "lodash";

const ReviewType = (walk?: Walk) => {
  const form = new FormBuilder()
    .setTheme(theme)
    .setTranslate(Translator)
    .createForm();
  form.add("walk", "WalkReviewField", {
    label: "walk",
    multiline: true,
    walk: walk,
    validators: [[(v: any) => !!v || isNumber(v), "form.validators.required"]],
  });
  if (walk) {
    walk?.walk_subscriptions?.forEach((walkSubscription: WalkSubscription) => {
      const id = (walkSubscription.dog as Dog).id;
      form.add(`${id}`, "DogReviewField", {
        label: "Dogs",
        multiline: true,
        dog: walkSubscription.dog as Dog,
        validators: [
          [(v: any) => !!v || isNumber(v), "form.validators.required"],
        ],
      });
    });
  }
  form.add("submit", "Button", {
    type: "submit",
    variant: "contained",
    endIcon: <SendIcon />,
    children: walk ? i18next.t("save") : i18next.t("create"),
  });
  return form;
};

export default ReviewType;

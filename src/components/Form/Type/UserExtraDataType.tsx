import theme from "../theme";
import Translator from "../translate";
import FormBuilder from "@elevenlabs/react-formbuilder";
import TextField from "../@Field/TextField";
import { UserExtraData } from "../../../interfaces/userextradata";
import i18next from "i18next";
import SendIcon from "@mui/icons-material/Send";
import React from "react";
import Slider from "../@Field/Slider";

const UserExtraDataType = (userExtraData?: UserExtraData) => {
  return new FormBuilder()
    .setTheme(theme)
    .setTranslate(Translator)
    .createForm()

    .add("travel_area", "FieldWithTitle", {
        field: Slider,
      label: "user_extra_data.travel_area.label",
      help: "user_extra_data.travel_area.help",
      min: 0,
      max: 50,
      step: 1,
      marks: [
        { value: 0, label: "0km" },
        { value: 50, label: "50km" },
      ],
      valueLabelDisplay: "auto",
      initialValue: userExtraData?.travel_area ?? 1,
        title: "user_extra_data.travel_area.title"
    })
    .add("positive_way", "FieldWithTitle", {
        field: Slider,
      label: "user_extra_data.positive_way.label",
      min: 0,
      max: 10,
      step: 1,
      marks: true,
      valueLabelDisplay: "auto",
      initialValue: userExtraData?.positive_way ?? 1,
        title: "user_extra_data.positive_way.title",
        help: "user_extra_data.positive_way.help"
    })
    .add("natural_way", "Slider", {
      label: "user_extra_data.natural_way.label",
      min: 0,
      max: 10,
      step: 1,
      marks: true,
      valueLabelDisplay: "auto",
      initialValue: userExtraData?.natural_way ?? 1,
        help: "user_extra_data.natural_way.help"
    })
    .add("traditional_way", "Slider", {
      label: "user_extra_data.traditional_way.label",
      min: 0,
      max: 10,
      step: 1,
      marks: true,
      valueLabelDisplay: "auto",
      initialValue: userExtraData?.traditional_way ?? 1,
        help: "user_extra_data.traditional_way.help"
    })
    .add("resume", "FieldWithTitle", {
      field: TextField,
      label: "user_extra_data.resume.label",
      initialValue: userExtraData?.resume,
      multiline: true,
      rows: 4,
        title: "user_extra_data.resume.title"
    })
    .add("submit", "Button", {
      type: "submit",
      variant: "contained",
      endIcon: <SendIcon />,
      children: i18next.t("save"),
    });
};

export default UserExtraDataType;

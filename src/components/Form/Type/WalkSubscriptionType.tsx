import theme from "../theme";
import Translator from "../translate";
import FormBuilder from "@elevenlabs/react-formbuilder";
import React from "react";
import { Walk } from "../../../interfaces/walk";
import SendIcon from "@mui/icons-material/Send";
import i18next from "i18next";
import { WalkSubscription } from "../../../interfaces/walksubscription";

const WalkSubscriptionType = (
  selectableDogs?: WalkSubscription[],
  walk?: Walk
) => {
  const form = new FormBuilder()
    .setTheme(theme)
    .setTranslate(Translator)
    .createForm();
  form
    .add("walk_subscriptions", "Select", {
      label: "walk.walk_subscriptions",
      validators: [
        [
          (v: any) => {
            return v.length > 0;
          },
          "form.validators.at_least_one",
        ],
      ],
      multiple: true,
      menuItems: selectableDogs,
      initialValue: [],
    })
    .add("submit", "Button", {
      type: "submit",
      variant: "contained",
      endIcon: <SendIcon />,
      children: walk ? i18next.t("save") : i18next.t("create"),
    });
  return form;
};

export default WalkSubscriptionType;

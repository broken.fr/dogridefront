import theme from "../theme";
import Translator from "../translate";
import FormBuilder from "@elevenlabs/react-formbuilder";
import React from "react";
import { Walk } from "../../../interfaces/walk";
import SendIcon from "@mui/icons-material/Send";
import i18next from "i18next";
import { WalkSubscription } from "../../../interfaces/walksubscription";
import TextField from "../@Field/TextField";

const WalkType = (selectableDogs?: WalkSubscription[], walk?: Walk) => {
  const form = new FormBuilder()
      .setTheme(theme)
      .setTranslate(Translator)
      .createForm()
      .add("coordinates", "MapField", {
        label: "walk.coordinates",
        required: true,
        validators: ["Required"],
        initialValue:
            walk?.latitude && walk?.longitude
                ? [walk.latitude, walk.longitude]
                : undefined,
      })

      .add("walk_type", "Select", {
        label: "walk.walk_type.label",
        validators: [
          [(v: any) => !!v || v === false, "form.validators.required"],
        ],
        menuItems: [
          { value: "forest_walk", label: "walk.walk_type.forest_walk" },
          { value: "field_walk", label: "walk.walk_type.field_walk" },
          {
            value: "town_walk",
            label: "walk.walk_type.town_walk",
          },
          { value: "canicross", label: "walk.walk_type.canicross" },
          { value: "canivtt", label: "walk.walk_type.canivtt" },
        ],
        initialValue: walk?.walk_type,
      })
      .add("participants_max", "Slider", {
        label: "walk.participants_max",
        min: 1,
        max: 8,
        step: 1,
        marks: true,
        valueLabelDisplay: "auto",
        initialValue: walk?.participants_max ?? 8,
      })
      .add("walk_date", "MobileDateTimePicker", {
        label: "walk.walk_date",
        validators: ["Required"],
        disablePast: true,
        initialValue: walk?.walk_date,
      })
      .add("walk_duration", "Slider", {
        label: "walk.walk_duration",
        min: 5,
        max: 120,
        step: 5,
        marks: true,
        valueLabelDisplay: "auto",
        initialValue: walk?.walk_duration ?? 30,
      })
  if (!walk) {
    form.add("walk_subscriptions", "Select", {
      label: "walk.walk_subscriptions",
      validators: [
        [
          (v: any) => {
            return v.length > 0;
          },
          "form.validators.at_least_one",
        ],
      ],
      multiple: true,
      menuItems: selectableDogs,
      initialValue: [],
    });
  }
  form.add("rules_acceptance", "Checkbox", {
    label: "walk.rules_acceptance",
    required: true,
    validators: [[(v: any) => !!v, "form.validators.rules_acceptance"]],
    initialValue: walk?.rules_acceptance ?? false,
  })
  form.add("description", "FieldWithTitle", {
    field: TextField,
    label: "walk.description",
    initialValue: walk?.description,
    multiline: true,
    rows: 4,
  });
  form.add("submit", "Button", {
    type: "submit",
    variant: "contained",
    endIcon: <SendIcon />,
    children: walk ? i18next.t("save") : i18next.t("create"),
  });
  return form;
};

export default WalkType;

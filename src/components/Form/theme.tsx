import React from "react";
import TextField from "./@Field/TextField";
import FileInput from "./@Field/FileInput";
import Autocomplete from "./@Field/Autocomplete";
import Grid from "@mui/material/Grid";
import Select from "./@Field/Select";
import MobileDatePicker from "./@Field/MobileDatePicker";
import Slider from "./@Field/Slider";
import Checkbox from "./@Field/Checkbox";
import FieldWithTitle from "./@Field/FieldWithTitle";
import { FormControl } from "@mui/material";
import Button from "./@Field/Button";
import MobileDateTimePicker from "./@Field/MobileDateTimePicker";
import MapField from "./@Field/MapField";
import WalkReviewField from "./@Field/WalkReviewField";
import DogReviewField from "./@Field/DogReviewField";

const theme = {
  Form: ({
    children,
    onSubmit,
    className,
  }: {
    children: any;
    onSubmit: any;
    className: any;
  }) => {
    return (
      <form
        onSubmit={onSubmit}
        className={className}
        style={{ marginBottom: 30 }}
      >
        <Grid
          container
          spacing={5}
          key="grid"
          direction="row"
          justifyContent="center"
          alignItems="stretch"
          mt={2}
        >
          {children}
        </Grid>
      </form>
    );
  },
  Row: ({
    // Label,
    Field,
    Errors,
    fieldType,
    errors,
  }: // required,
  {
    Label: any;
    Field: any;
    Errors: any;
    fieldType: any;
    errors: any;
    required: any;
  }) => {
    const execField = new Field();
    const isFieldWithTitle = fieldType === "FieldWithTitle";
    const content = (
      <>
        {execField}
        <Errors />
      </>
    );
    if (
      ["Button", "MapField", "WalkReviewField", "DogReviewField"].includes(
        fieldType
      )
    ) {
      return (
        <Grid item xs={12} sm={12} md={12} lg={12}>
          {content}
        </Grid>
      );
    }
    if (!isFieldWithTitle) {
      return (
        <Grid item xs={10} sm={10} md={6} lg={4}>
          <FormControl error={Boolean(errors)} fullWidth>
            {content}
          </FormControl>
        </Grid>
      );
    }
    return content;
  },
  Label: ({ label, ...props }: { label: any; props: any }) => (
    <label {...props}>{label}</label>
  ),
  Errors: () => (
    <> </>
    // <ErrorMessage name={errors.name}>{errors}</ErrorMessage>
  ),

  TextField: (props: any) => <TextField {...props} />,
  FileInput: (props: any) => <FileInput {...props} />,
  Autocomplete: (props: any) => <Autocomplete {...props} />,
  MobileDatePicker: (props: any) => <MobileDatePicker {...props} />,
  MobileDateTimePicker: (props: any) => <MobileDateTimePicker {...props} />,
  Slider: (props: any) => <Slider {...props} />,
  Checkbox: (props: any) => <Checkbox {...props} />,
  FieldWithTitle: (props: any) => <FieldWithTitle {...props} />,
  Select: (props: any) => <Select {...props} />,
  Button: (props: any) => <Button {...props} />,
  MapField: (props: any) => <MapField {...props} />,
  WalkReviewField: (props: any) => <WalkReviewField {...props} />,
  DogReviewField: (props: any) => <DogReviewField {...props} />,
};

export default theme;

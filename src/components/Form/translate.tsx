import "../../i18n/config";
import i18next from "i18next";

const translate = (key?: any, args?: any) => {
  if (args.label) {
    args.label = i18next.t(args.label);
  }
  return i18next.t(key, args);
};

export default translate;

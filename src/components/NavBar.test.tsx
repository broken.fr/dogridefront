import React from 'react';
import {render, fireEvent, screen} from '@testing-library/react';
import {BrowserRouter as Router} from 'react-router-dom';
import NavBar from './NavBar';
import {Auth0ContextInterface, useAuth0} from "@auth0/auth0-react";
import '@testing-library/jest-dom'

// if you're using jest 27.4.0+ and ts-jest 28.0.0+
import {mocked} from "jest-mock";
import {User} from "../interfaces/user";

// if you're using ts-jest < 28.0.0
// import { mocked } from "ts-jest/utils";

const user: User = {
    email: "johndoe@me.com",
    email_verified: true,
    family_name: "Doe",
    username: "johndoe"
};

jest.mock("@auth0/auth0-react");
jest.mock('../assets/img/logo.png', () => 'logo.png');

const mockedUseAuth0 = mocked(useAuth0);

const renderNavBar = () => {
    return render(
        <Router>
            <NavBar/>
        </Router>
    );
};
const mock: Auth0ContextInterface<User> = {
    buildAuthorizeUrl: jest.fn(),
    buildLogoutUrl: jest.fn(),
    handleRedirectCallback: jest.fn(),
    isAuthenticated: true,
    user,
    logout: jest.fn(),
    loginWithRedirect: jest.fn(),
    getAccessTokenWithPopup: jest.fn(),
    getAccessTokenSilently: jest.fn(),
    getIdTokenClaims: jest.fn(),
    loginWithPopup: jest.fn(),
    isLoading: false
};
describe('NavBar component', () => {
    beforeEach(() => {
        mockedUseAuth0.mockReturnValue(mock);
    });
    test('renders without crashing', () => {
        renderNavBar();
    });

    test('displays the logo', () => {
        renderNavBar();
        const logos = screen.getAllByAltText('Dogride Logo');
        const logo = logos[0];

        expect(logo).toBeInTheDocument();
    });


    test('displays the user menu and can be opened', () => {
        renderNavBar();
        const userMenuButton = screen.getByLabelText('Open settings');
        expect(userMenuButton).toBeInTheDocument();

        fireEvent.click(userMenuButton);
        const userMenu = screen.getByRole('menu');
        expect(userMenu).toBeInTheDocument();
    });

    test('displays the theme switch and works correctly', () => {
        renderNavBar();
        const userMenuButton = screen.getByLabelText('Open settings');
        expect(userMenuButton).toBeInTheDocument();

        fireEvent.click(userMenuButton);
        const userMenu = screen.getByRole('menu');
        expect(userMenu).toBeInTheDocument();
        // render(<MaterialUISwitch />)
        const themeSwitch = screen.getByRole('checkbox');
        expect(themeSwitch).toBeInTheDocument();

        fireEvent.click(themeSwitch);
        expect(themeSwitch).not.toBeChecked();
    });
});
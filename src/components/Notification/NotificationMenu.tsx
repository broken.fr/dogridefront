import React, { useCallback, useEffect, useRef, useState } from "react";
import {
  Badge,
  Button,
  Divider,
  Grid,
  IconButton,
  ListItemText,
  Menu,
  MenuItem,
  Typography,
} from "@mui/material";
import NotificationsIcon from "@mui/icons-material/Notifications";
import { Notification } from "../../interfaces/notification";
import { AxiosConnection } from "../../lib/AxiosConnection";
import { Link } from "react-router-dom";
import Config from "../../config.json";
import { useUserExtraDataContext } from "../../lib/UserExtraDataContext";
import "../../i18n/config";
import { useTranslation } from "react-i18next";

export const NotificationMenu = () => {
  const [anchorEl, setAnchorEl] = useState<HTMLButtonElement | null>();
  const [notifications, setNotifications] = useState<Notification[]>();
  const [readNotifications, setReadNotifications] = useState<Notification[]>();
  const { getQuery } = AxiosConnection();
  const [hasMore, setHasMore] = useState<boolean>(false);
  const { userExtraData } = useUserExtraDataContext();
  const { t } = useTranslation();
  const { patchQuery } = AxiosConnection();
  const lastNotificationIdRef = useRef(null);

  const fetchNotifications = useCallback(() => {
    if (!userExtraData) {
      return;
    }
    const url = `notifications?itemsPerPage=10&recipient=${
      userExtraData.id
    }&order[created_at]=DESC&seen=false${
      lastNotificationIdRef.current
        ? `&last_id=${lastNotificationIdRef.current}`
        : ""
    }`;
    getQuery(url).then((response) => {
      const newNotifications = response.data["hydra:member"];
      if (newNotifications.length) {
        setNotifications((notifications) => {
          if (!notifications) {
            return newNotifications;
          }
          // On filtre les nouvelles notifications qui ont déjà un id présent dans la liste des notifications actuelles
          const filteredNewNotifications = newNotifications.filter(
            (notification: Notification) =>
              !notifications.some((n) => n.id === notification.id)
          );
          // On concatène la liste filtrée de nouvelles notifications à la liste actuelle
          return [...notifications, ...filteredNewNotifications];
        });
        lastNotificationIdRef.current =
          newNotifications[newNotifications.length - 1].id;
      }
      response.data["hydra:view"]["hydra:next"]
        ? setHasMore(true)
        : setHasMore(false);
    });
  }, [userExtraData, getQuery]);

  const fetchReadNotifications = useCallback(() => {
    if (!userExtraData) {
      return;
    }
    const url = `notifications?itemsPerPage=10&recipient=${userExtraData.id}&order[created_at]=DESC&seen=1`;
    getQuery(url).then((response) => {
      const newNotifications = response.data["hydra:member"];
      if (newNotifications.length) {
        setReadNotifications((readNotifications) => {
          if (!readNotifications) {
            return newNotifications;
          }
          // On filtre les nouvelles notifications qui ont déjà un id présent dans la liste des notifications actuelles
          const filteredNewNotifications = newNotifications.filter(
            (notification: Notification) =>
              !readNotifications.some((n) => n.id === notification.id)
          );
          // On concatène la liste filtrée de nouvelles notifications à la liste actuelle
          return [...readNotifications, ...filteredNewNotifications];
        });
      }
    });
  }, [getQuery, userExtraData]);

  const subscribeToNotifications = useCallback(() => {
    if (!userExtraData) {
      return;
    }
    const mercureUrl = `${Config.REACT_APP_DOGGOBOX_URL}/.well-known/mercure?topic=/api/notifications?recipient=${userExtraData.id}`;
    const eventSource = new EventSource(mercureUrl);

    eventSource.addEventListener(
      "notification.created",
      (event: MessageEvent) => {
        const eventNotification: Notification = JSON.parse(event.data);
        if (eventNotification.seen) {
          setNotifications((notifications) => {
            if (!notifications) {
              return [];
            }

            return notifications.filter(
              (notification) => notification.id !== eventNotification.id
            );
          });
          return;
        }
        setNotifications((notifications) => {
          if (!notifications) {
            return [eventNotification];
          }

          return [eventNotification, ...notifications];
        });
      }
    );

    return () => {
      if (eventSource) {
        eventSource.close();
      }
    };
  }, [userExtraData]);

  useEffect(() => {
    fetchNotifications();
    subscribeToNotifications();
    fetchReadNotifications();
  }, [subscribeToNotifications, fetchReadNotifications, fetchNotifications]);

  useEffect(() => {
    if (notifications && notifications.length < 5 && hasMore) {
      fetchNotifications();
    }
  }, [notifications, hasMore, fetchNotifications]);

  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const markAsSeen = async (event: React.MouseEvent<HTMLElement>) => {
    const notificationId = event.currentTarget.dataset.id;
    const updatedNotifications = notifications?.filter(
      (notification: Notification) => notification.id !== notificationId
    );
    const updatedReadNotification = notifications?.filter(
      (notification: Notification) => notification.id === notificationId
    );
    if (updatedNotifications) {
      setNotifications(updatedNotifications);
      setReadNotifications((readNotifications) => {
        if (!readNotifications) {
          return updatedReadNotification;
        }
        if (!updatedReadNotification) {
          return readNotifications;
        }
        return [...updatedReadNotification, ...readNotifications];
      });
      await patchQuery(`notifications/${notificationId}`, { seen: true });
    }
  };

  const NotificationDirectory = {
    walk_finished: (notification: Notification) => {
      return (
        <Button
          component={Link}
          to={`/walk/review/${notification.related_entity_id}`}
        >
          Noter la balade
        </Button>
      );
    },
    walk_subscription_updated: (notification: Notification) => {
      return (
        <Button
          component={Link}
          to={`/walk/view/${notification.related_entity_id}`}
        >
          Voir la balade
        </Button>
      );
    },
  };

  function notifier(
    notification: Notification,
    notificationType: keyof typeof NotificationDirectory
  ) {
    const handler = NotificationDirectory[notificationType];
    if (!handler)
      throw new Error(`No handler for notification type ${notificationType}`);
    if (!(typeof handler === "function"))
      throw new Error(
        `Handler for notification type ${notificationType} is not a function`
      );

    return handler(notification);
  }

  return (
    <>
      <IconButton
        onClick={handleClick}
        sx={{ marginRight: 1 }}
        aria-controls={Boolean(anchorEl) ? "basic-menu" : undefined}
        aria-haspopup="true"
        aria-expanded={Boolean(anchorEl) ? "true" : undefined}
        size="medium"
      >
        <Badge
          badgeContent={
            notifications?.map(
              (notification: Notification) => notification.seen
            ).length || 0
          }
          max={
            hasMore
              ? (notifications?.map(
                  (notification: Notification) => notification.seen
                ).length ?? 11) - 1
              : 10
          }
          color="error"
        >
          <NotificationsIcon fontSize="large" />
        </Badge>
      </IconButton>
      <Menu
        id="basic-menu"
        anchorEl={anchorEl}
        open={Boolean(anchorEl)}
        onClose={handleClose}
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "left",
        }}
        transformOrigin={{
          vertical: "top",
          horizontal: "left",
        }}
        sx={{ p: 0, marginTop: 3 }}
      >
        <Typography
          sx={{ paddingLeft: 2, paddingRight: 2 }}
          variant="h5"
          gutterBottom
        >
          Notifications
        </Typography>
        <Divider />
        {notifications && (
          <div>
            {notifications?.map((notification) => {
              return (
                <MenuItem
                  key={notification.id}
                  data-id={notification.id}
                  onClick={markAsSeen}
                >
                  <Grid container>
                    <Grid item xs={12}>
                      <Badge
                        sx={{ width: "100%" }}
                        color="error"
                        variant="dot"
                        anchorOrigin={{
                          vertical: "bottom",
                          horizontal: "left",
                        }}
                      >
                        <ListItemText inset>
                          {t(`notification.messages.${notification.message}`)}
                        </ListItemText>
                      </Badge>
                    </Grid>
                    <Grid item xs={12}>
                      <Grid
                        container
                        justifyContent="flex-end"
                        alignItems="flex-end"
                      >
                        <Grid item>
                          {notifier(
                            notification,
                            notification.type as
                              | "walk_finished"
                              | "walk_subscription_updated"
                          )}
                        </Grid>
                      </Grid>
                    </Grid>
                  </Grid>
                </MenuItem>
              );
            })}
          </div>
        )}
        {readNotifications && (
          <div>
            {readNotifications?.map((notification: Notification) => {
              return (
                <MenuItem key={notification.id} data-id={notification.id}>
                  <Grid container>
                    <Grid item xs={12}>
                      <ListItemText inset>
                        {t(`notification.messages.${notification.message}`)}
                      </ListItemText>
                    </Grid>
                    <Grid item xs={12}>
                      <Grid
                        container
                        justifyContent="flex-end"
                        alignItems="flex-end"
                      >
                        <Grid item>
                          {notifier(
                            notification,
                            notification.type as
                              | "walk_finished"
                              | "walk_subscription_updated"
                          )}
                        </Grid>
                      </Grid>
                    </Grid>
                  </Grid>
                </MenuItem>
              );
            })}
          </div>
        )}
        {!notifications && !readNotifications && (
          <div>
            <Typography sx={{ padding: 2 }} variant="subtitle1">
              Il n'y a pas de notifications pour le moment
            </Typography>
          </div>
        )}
      </Menu>
    </>
  );
};

import React from "react";
import {Typography} from "@mui/material";

const Recommendations = () => {

    return (
        <>
            <Typography variant={"h1"}>Préconisations pour une Balade</Typography>
            <Typography mb={1} mt={2}>
                La balade de chiens en groupe est une expérience amusante et enrichissante pour nos compagnons canins. Cependant, pour assurer la sécurité et le bien-être de tous les participants, il est important de respecter certaines préconisations. Voici quelques conseils pour des balades canines en groupe réussies.
            </Typography>
            <Typography variant={"h3"} fontSize={"20px"} mb={1} mt={2}>
                Vérifier la compatibilité des chiens :
            </Typography>
            <Typography>
                Avant de rejoindre une balade en groupe, il est crucial de s'assurer de la compatibilité des chiens. Les tempéraments, la taille, l'énergie et les besoins spécifiques de chaque chien doivent être pris en compte. Une bonne entente entre les chiens favorisera une expérience agréable pour tous.
            </Typography>
            <Typography variant={"h3"} fontSize={"20px"} mb={1} mt={2}>
                Assurer une bonne socialisation :
            </Typography>
            <Typography>
                Des chiens bien socialisés ont généralement de meilleures interactions avec leurs congénères. Avant de participer à une balade en groupe, il est recommandé de travailler sur la socialisation de votre chien. Les interactions positives avec d'autres chiens contribueront à des rencontres harmonieuses pendant la balade.
            </Typography>
            <Typography variant={"h3"} fontSize={"20px"} mb={1} mt={2}>
                Tenir les chiens en laisse au départ :
            </Typography>
            <Typography>
                Au début de la balade, il est préférable de garder tous les chiens en laisse pour permettre aux maîtres de faire connaissance et d'établir une dynamique de groupe. Cela évite également les comportements indésirables et assure un démarrage en douceur.
            </Typography>
            <Typography variant={"h3"} fontSize={"20px"} mb={1} mt={2}>
                Respecter les règles de la balade :
            </Typography>
            <Typography>
                Il est essentiel de respecter les règles établies par l'organisateur de la balade. Cela peut inclure des consignes concernant la laisse, les zones de jeu autorisées, les comportements attendus, etc. Ces règles garantissent la sécurité et le bon déroulement de la balade pour tous les participants.
            </Typography>
            <Typography variant={"h3"} fontSize={"20px"} mb={1} mt={2}>
                Surveiller les signaux corporels des chiens :
            </Typography>
            <Typography>
                Pendant la balade, observez attentivement les signaux corporels de votre chien et des autres. Apprenez à reconnaître les signes de stress, d'inconfort ou d'agitation. Si vous identifiez des tensions ou des conflits potentiels, éloignez-vous prudemment pour éviter tout problème.
            </Typography>
            <Typography variant={"h3"} fontSize={"20px"} mb={1} mt={2}>
                Ramasser les déjections canines :
            </Typography>
            <Typography>
                Assurez-vous de toujours avoir des sacs pour ramasser les déjections de votre chien pendant la balade. Il est de notre responsabilité de maintenir les espaces publics propres et respectueux de l'environnement.
            </Typography>
            <Typography variant={"h3"} fontSize={"20px"} mb={1} mt={2}>
                Important :
            </Typography>
            <Typography>
                DogRide n'est pas responsable en cas de problèmes, blessures ou dommages pouvant survenir lors des événements organisés par ses utilisateurs. Chaque participant est responsable de la surveillance et de la sécurité de son chien pendant la balade. Veuillez agir de manière responsable et respecter les règles de l'événement.
            </Typography>
            <Typography mt={3}>
                En suivant ces préconisations, vous contribuerez à créer un environnement sûr et harmonieux lors des balades de chiens en groupe. Profitez de ces moments de socialisation, de partage et d'amusement avec votre fidèle compagnon tout en cultivant de belles amitiés canines !
            </Typography>
        </>
    );
};
export default Recommendations;

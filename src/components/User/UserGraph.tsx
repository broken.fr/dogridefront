import React from 'react';
import { useTheme } from "@mui/material";
import {UserExtraData} from "../../interfaces/userextradata";
import { Radar, RadarChart, PolarGrid, PolarAngleAxis, ResponsiveContainer, Legend } from 'recharts';

interface ChartData {
    subject: string;
    A: number;
    fullMark: number;
}

export const UserGraph = ({ userExtraData }:{ userExtraData: UserExtraData }) => {
    const theme = useTheme();
    const data: ChartData[] = [
        {
            subject: 'Positive',
            A: userExtraData?.positive_way ?? 0,
            fullMark: 5,
        },
        {
            subject: 'Communication',
            A: userExtraData?.natural_way ?? 0,
            fullMark: 5,
        },
        {
            subject: 'Traditionnelle ',
            A: userExtraData?.traditional_way ?? 0,
            fullMark: 5,
        }
    ];
    const startColor = '#1f7ee8';
    const stopColor = '#71c5f8';

    return (
        <ResponsiveContainer width="100%" height="100%">
            <RadarChart cx="50%" cy="50%" outerRadius="90%" data={data}>
                <defs>
                    <linearGradient id="grad1" x1="0%" y1="0%" x2="100%" y2="0%">
                        <stop offset="0%" stopColor={startColor}/>
                        <stop offset="100%" stopColor={stopColor}/>
                    </linearGradient>
                </defs>
                <PolarGrid />
                <PolarAngleAxis dataKey="subject" fontSize={"15px"} />
                <Radar
                    name="Type d'education"
                    dataKey="A"
                    stroke={theme.palette.primary.main}
                    fill="url(#grad1)"
                    fillOpacity={0.7}
                />
                {/*<Legend />*/}
            </RadarChart>
        </ResponsiveContainer>
    );
}

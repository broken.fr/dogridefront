import {User} from "../../interfaces/user";
import {Avatar, Box, Card, CardContent, Chip, Fab, Typography, useTheme} from "@mui/material";
import {Link} from "react-router-dom";
import {LocationOn} from "@mui/icons-material";
import Grid from "@mui/material/Grid";
import {UserGraph} from "./UserGraph";
import React from "react";
import {UserExtraData} from "../../interfaces/userextradata";

const UserInfo = ({ user }:{ user: User }) => {
    const theme = useTheme();
    return (
        <>
            <Grid container spacing={2} justifyContent="space-between">
                <Grid item xs={12} md={4}>
                    <Box sx={{ width: "100%", overflow: "visible" }}>
                        <Card
                            sx={{ overflow: "visible", background: theme.palette.background.default }}
                        >
                            <Box
                                style={{
                                    width: "180px",
                                    height: "180px",
                                    margin: "90px auto -90px auto",
                                    position: "relative",
                                }}
                            >
                                <Avatar
                                    src={user?.picture}
                                    alt="Image de profil"
                                    sx={{
                                        width: "100%",
                                        position: "absolute",
                                        top: "-90px",
                                        height: "100%",
                                        display: "block",
                                        border: "10px solid " + theme.palette.background.paper,
                                    }}
                                />
                            </Box>
                            <CardContent >
                                <Typography
                                    align={"center"}
                                    gutterBottom
                                    variant="h5"
                                    component="div"
                                >
                                    {user?.username.charAt(0).toUpperCase()}
                                    {user?.username.slice(1)}
                                </Typography>
                                <Typography
                                    gutterBottom
                                    component="div"
                                    m={3}
                                >
                                    {(user?.user_extra_data as UserExtraData)?.resume}
                                </Typography>
                                <Chip
                                    color="primary"
                                    icon={<LocationOn />}
                                    label={
                                        "Zone de déplacement : " +
                                        (user?.user_extra_data as UserExtraData)?.travel_area +
                                        "km"
                                    }
                                />
                            </CardContent>
                        </Card>
                    </Box>
                </Grid>
                <Grid
                    container
                    item
                    xs={12}
                    md={7}
                    direction="column"
                    alignItems="stretch"
                >
                    <Grid item>
                        <Grid
                            container
                            direction="row"
                            justifyContent="center"
                            alignItems="flex-start"
                            sx={{ mt: 5 }}
                        >
                            <Grid item xs={11} md={12} style={{ height: "433px" }}>
                                <UserGraph userExtraData={(user?.user_extra_data as UserExtraData)} />
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
        </>
    )
};

export default UserInfo;
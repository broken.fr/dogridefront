import { Walk } from "../../interfaces/walk";
import { UserExtraData } from "../../interfaces/userextradata";
import {
  Backdrop,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  Fab,
  SpeedDial,
  SpeedDialAction,
  SpeedDialIcon,
  useMediaQuery,
  useTheme,
} from "@mui/material";
import { Add, Edit, Star } from "@mui/icons-material";
import React, { useState } from "react";
import DeleteIcon from "@mui/icons-material/Delete";
import { useAbilityContext, useUserContext } from "../../stores/casl/store";
import { OpenReason } from "@mui/material/SpeedDial/SpeedDial";
import { toast } from "react-toastify";
import { useCustomBackdrop } from "../../lib/CustomBackdrop";
import { Link, useNavigate } from "react-router-dom";
import { AxiosConnection } from "../../lib/AxiosConnection";
import { useTranslation } from "react-i18next";
import AddIcon from "@mui/icons-material/Add";
import moment, { Moment } from "moment";

const actions: { icon: JSX.Element; name: string; action: string }[] = [
  { icon: <DeleteIcon />, name: "Supprimer", action: "delete" },
  { icon: <Add />, name: "S'inscrire", action: "register" },
];
const WalkActions = ({ walk }: { walk: Walk }) => {
  const [open, setOpen] = useState<boolean>(false);
  const [dialogOpen, setDialogOpen] = useState<boolean>(false);
  const user = useUserContext();
  const ability = useAbilityContext();
  const theme = useTheme();
  const { deleteQuery } = AxiosConnection();
  const fullScreen = useMediaQuery(theme.breakpoints.down("md"));
  const { t } = useTranslation();
  const { displayBackdrop, closeBackdrop } = useCustomBackdrop();
  const navigate = useNavigate();
  const numberOfAcceptedParticipants = walk?.walk_subscriptions?.reduce(
    (acceptedTotal, walk_subscriptions) =>
      walk_subscriptions.accepted === true ? ++acceptedTotal : acceptedTotal,
    0
  );
  const isFull = numberOfAcceptedParticipants
    ? numberOfAcceptedParticipants >= (walk.participants_max as number)
    : false;

  const handleOpen = (event: React.SyntheticEvent<{}>, reason: OpenReason) => {
    if (reason === "focus") return;
    setOpen(true);
  };
  const handleClose = (event: React.SyntheticEvent<{}>, reason: string) => {
    setOpen(false);
    if (reason === "delete") {
      setDialogOpen(true);
    }
    if (reason === "register") {
      navigate("/walk/subscribe/" + walk.id);
    }
    if (reason === "toggle") {
      navigate("/walk/edit/" + walk.id);
    }
  };

  const handleDialogClose = (action: boolean) => {
    setDialogOpen(false);
    if (!walk) return;
    if (action) {
      displayBackdrop();
      const walkId = walk.id;
      deleteQuery("walks/" + walkId)
        .then(() => {
          toast.success(t("walk.delete.success"), {
            position: "bottom-right",
            autoClose: 3000,
            hideProgressBar: true,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            toastId: "delete-walk-infos-success",
          });
          navigate("/");
        })
        .catch(() => {
          toast.error(t("walk.delete.error"), {
            position: "bottom-right",
            autoClose: 3000,
            hideProgressBar: true,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            toastId: "delete-walk-infos-error",
          });
        })
        .finally(() => {
          closeBackdrop();
        });
    }
  };

  // console.log((walk?.walk_date as Moment).isAfter(moment()));

  return (
    <>
      {(user &&
        (walk?.owner as UserExtraData).user === user?.["@id"] &&
        (walk?.walk_date as Moment).isAfter(moment())) ||
      ability.can("delete", "walk") ? (
        <>
          <Backdrop
            open={open || dialogOpen}
            sx={{ color: "#fff", zIndex: 1001 }}
          />
          <SpeedDial
            ariaLabel="SpeedDial tooltip example"
            sx={{ position: "fixed", bottom: 16, right: 16 }}
            icon={<SpeedDialIcon icon={<Edit />} />}
            onClose={handleClose}
            onOpen={handleOpen}
            open={open}
          >
            {actions.map((action) => (
              <SpeedDialAction
                key={action.name}
                icon={action.icon}
                tooltipTitle={action.name}
                tooltipOpen
                onClick={(e) => handleClose(e, action.action)}
              />
            ))}
          </SpeedDial>
          <Dialog
            fullScreen={fullScreen}
            open={dialogOpen}
            onClose={() => handleDialogClose(false)}
            aria-labelledby="responsive-dialog-title"
          >
            <DialogTitle id="responsive-dialog-title">
              {t("walk.delete.dialog.title")}
            </DialogTitle>
            <DialogContent>
              <DialogContentText>
                {t("walk.delete.dialog.message")}
              </DialogContentText>
            </DialogContent>
            <DialogActions>
              <Button autoFocus onClick={() => handleDialogClose(false)}>
                {t("walk.delete.dialog.cancel")}
              </Button>
              <Button onClick={() => handleDialogClose(true)} autoFocus>
                {t("walk.delete.dialog.confirm")}
              </Button>
            </DialogActions>
          </Dialog>
        </>
      ) : (walk?.walk_date as Moment).isBefore(moment()) ? (
        <Fab
          variant="extended"
          sx={{ position: "fixed", bottom: 20, right: 20 }}
          aria-label="Add"
          color="primary"
          component={Link}
          to={"/walk/review/" + walk.id}
        >
          <Star sx={{ mr: 1 }} />
          {t("review.label")}
        </Fab>
      ) : !isFull ? (
        <Fab
          variant="extended"
          sx={{ position: "fixed", bottom: 20, right: 20 }}
          aria-label="Add"
          color="primary"
          component={Link}
          to={"/walk/subscribe/" + walk.id}
        >
          <AddIcon sx={{ mr: 1 }} />
          {t("walk.subscribe")}
        </Fab>
      ) : (
        <></>
      )}
    </>
  );
};
export default WalkActions;

import React, { ElementType } from "react";
import { Avatar, Button, Grid, Typography } from "@mui/material";
import { WalkSubscription } from "../../interfaces/walksubscription";
import Config from "../../config.json";
import { Dog } from "../../interfaces/dog";
import { MediaObject } from "../../interfaces/mediaobject";
import { Breed } from "../../interfaces/breed";
import moment from "moment";
import { Link } from "react-router-dom";
import PetsIcon from "@mui/icons-material/Pets";
import GenderIcon from "@mui/icons-material/EmojiPeople";
import CalendarTodayIcon from "@mui/icons-material/CalendarToday";
import BloodtypeIcon from "@mui/icons-material/Bloodtype";
import { AxiosConnection } from "../../lib/AxiosConnection";
import { toast } from "react-toastify";
import { useCustomBackdrop } from "../../lib/CustomBackdrop";
import { useAbilityContext, useUserContext } from "../../stores/casl/store";
import { UserExtraData } from "../../interfaces/userextradata";

export const WalkSubscribers = ({
  walkSubscriptions,
  setWalkSubscriptions,
  owner,
  maxParticipants,
}: {
  walkSubscriptions: WalkSubscription[];
  setWalkSubscriptions: (walkSubscriptions: WalkSubscription[]) => void;
  owner: UserExtraData;
  maxParticipants: number;
}) => {
  // const [walkSubscriptions, setWalkSubscriptions] = useState<WalkSubscription[]>(walkSubscriptions);
  const { patchQuery, deleteQuery } = AxiosConnection();
  const { displayBackdrop, closeBackdrop } = useCustomBackdrop();
  const user = useUserContext();
  const ability = useAbilityContext();
  const numberOfAcceptedParticipants = walkSubscriptions.reduce(
    (acceptedTotal, walk_subscriptions) =>
      walk_subscriptions.accepted === true ? ++acceptedTotal : acceptedTotal,
    0
  );
  const isFull = numberOfAcceptedParticipants >= maxParticipants;

  const getAgeString = (birthdate: moment.Moment): string => {
    const ageYears = moment().diff(birthdate, "years");
    const ageMonths = moment().diff(birthdate, "months") % 12;
    return ageYears > 0
      ? `${ageYears} an${ageYears > 1 ? "s" : ""}`
      : `${ageMonths} mois`;
  };

  const acceptDog = (event: React.MouseEvent<HTMLButtonElement>) => {
    displayBackdrop();
    const walkId = event.currentTarget.dataset.id;
    patchQuery("walk_subscriptions/" + walkId, {
      accepted: true,
    })
      .then(() => {
        const updatedWalkSubscriptions = [...walkSubscriptions];
        const index = updatedWalkSubscriptions.findIndex(
          (walkSubscription: WalkSubscription) => walkSubscription.id === walkId
        );
        updatedWalkSubscriptions[index] = {
          ...updatedWalkSubscriptions[index],
          accepted: true,
        };
        toast.success("Le chien à bien été accepté dans la balade", {
          position: "bottom-right",
          autoClose: 3000,
          hideProgressBar: true,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          toastId: "get-walk-infos-error",
        });
      })
      .catch(() => {
        toast.error("Impossible d'accepter le chien dans la balade", {
          position: "bottom-right",
          autoClose: 3000,
          hideProgressBar: true,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          toastId: "get-walk-infos-error",
        });
      })
      .finally(() => {
        closeBackdrop();
      });
  };

  const refuseDog = (event: React.MouseEvent<HTMLButtonElement>) => {
    displayBackdrop();
    const walkSubscriptionId = event.currentTarget.dataset.id;

    const index = walkSubscriptions.findIndex(
      (walkSubscription: WalkSubscription) =>
        walkSubscription.id === walkSubscriptionId
    );
    const walkSubscription: WalkSubscription = walkSubscriptions[index];
    const currentUserDogIds: Dog[] | undefined = (
      user?.user_extra_data as UserExtraData
    )?.dogs;
    const isItDogOfUser: boolean = !currentUserDogIds
      ? false
      : currentUserDogIds?.findIndex(
          (dog: Dog): boolean => dog?.id === (walkSubscription?.dog as Dog)?.id
        ) > -1;
    deleteQuery("walk_subscriptions/" + walkSubscriptionId)
      .then(() => {
        const updatedWalkSubscriptions = walkSubscriptions.filter(
          (walkSubscription: WalkSubscription) =>
            walkSubscription.id !== walkSubscriptionId
        );
        setWalkSubscriptions(updatedWalkSubscriptions);
        const message = isItDogOfUser
          ? "Le chien à bien été refusé pour cette balade"
          : "Le chien à bien été retiré de la balade";
        toast.success(message, {
          position: "bottom-right",
          autoClose: 3000,
          hideProgressBar: true,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          toastId: "get-walk-infos-error",
        });
      })
      .catch(() => {
        const message = isItDogOfUser
          ? "Impossible de refuser le chien pour cette balade"
          : "Impossible de retirer le chien de la balade";
        toast.error(message, {
          position: "bottom-right",
          autoClose: 3000,
          hideProgressBar: true,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          toastId: "get-walk-infos-error",
        });
      })
      .finally(() => {
        closeBackdrop();
      });
  };

  return (
    <>
      {walkSubscriptions.map((walkSubscription: WalkSubscription) => {
        if (typeof walkSubscription.dog === "string") {
          return "";
        }

        const dog: Dog = walkSubscription.dog as Dog;
        const birthdate = moment(dog.birthdate);
        const ageString = getAgeString(birthdate);
        const ownersDogSubscribed = owner.dogs?.filter((dog: Dog) =>
          walkSubscriptions.find(
            (walkSubscription: WalkSubscription) =>
              (walkSubscription.dog as Dog).id === dog.id
          )
        );
        return (
          <Grid
            container
            key={walkSubscription["@id"]}
            sx={{ my: 3 }}
            alignItems="center"
            spacing={2}
          >
            <Grid item xs={12} sm={3} justifyContent="center">
              <Grid
                container
                spacing={2}
                alignItems="center"
                justifyContent="center"
              >
                <Grid item>
                  <Avatar
                    variant="circular"
                    src={
                      Config.REACT_APP_DOGGOBOX_URL +
                      (dog?.image as MediaObject)?.content_url
                    }
                    sx={{ width: 150, height: 150 }}
                  />
                </Grid>
              </Grid>
            </Grid>

            <Grid item xs={12} sm={6}>
              <Grid container spacing={2} alignItems="center">
                <Grid item>
                  <PetsIcon sx={{ fontSize: "2.5rem", color: "#6b7280" }} />
                </Grid>
                <Grid item>
                  <Typography variant="subtitle2" sx={{ fontWeight: "bold" }}>
                    {dog?.name}
                  </Typography>
                </Grid>
              </Grid>

              <Grid container spacing={2} alignItems="center">
                <Grid item>
                  <GenderIcon sx={{ fontSize: "2.5rem", color: "#6b7280" }} />
                </Grid>
                <Grid item>
                  <Typography variant="subtitle2" sx={{ fontWeight: "bold" }}>
                    {dog?.gender === "female" ? "Femelle" : "Male"}
                  </Typography>
                </Grid>
              </Grid>

              <Grid container spacing={2} alignItems="center">
                <Grid item>
                  <BloodtypeIcon
                    sx={{ fontSize: "2.5rem", color: "#6b7280" }}
                  />
                </Grid>
                <Grid item>
                  <Typography variant="subtitle2" sx={{ fontWeight: "bold" }}>
                    {(dog?.breed as Breed)?.name}
                  </Typography>
                </Grid>
              </Grid>

              <Grid container spacing={2} alignItems="center">
                <Grid item>
                  <CalendarTodayIcon
                    sx={{ fontSize: "2.5rem", color: "#6b7280" }}
                  />
                </Grid>
                <Grid item>
                  <Typography variant="subtitle2" sx={{ fontWeight: "bold" }}>
                    {ageString}
                  </Typography>
                </Grid>
              </Grid>
            </Grid>

            <Grid item xs={12} sm={3}>
              <Grid
                container
                spacing={2}
                direction={"column"}
                justifyContent={"center"}
                alignItems={"flex-end"}
              >
                <Grid item xs={12}>
                  <Button
                    size="small"
                    color="primary"
                    component={Link as ElementType}
                    to={"/dog/profile/" + (walkSubscription.dog as Dog).id}
                  >
                    Profil du poilu
                  </Button>
                </Grid>
                <>
                  {walkSubscription.accepted === false &&
                    (owner.user === user?.["@id"] ||
                      ability.can("update", "walks")) &&
                    !isFull && (
                      <Grid item xs={12}>
                        <Button
                          size="small"
                          data-id={walkSubscription.id}
                          color="primary"
                          onClick={acceptDog}
                        >
                          Accepter
                        </Button>
                      </Grid>
                    )}
                </>
                <>
                  {(ability.can("delete", "walk_subscriptions") ||
                    owner.user === user?.["@id"]) &&
                    /* Le chien n'est pas dans la liste des chiens de propriétaire de la balade */
                    (owner.dogs?.findIndex(
                      (dog: Dog) => dog.id === (walkSubscription.dog as Dog).id
                    ) === -1 ||
                      // Le propriétaire de la balade à plus d'un chien inscrit à la balade
                      (ownersDogSubscribed &&
                        ownersDogSubscribed.length > 1)) && (
                      <Grid item xs={12}>
                        <Button
                          size="small"
                          data-id={walkSubscription.id}
                          color="error"
                          onClick={refuseDog}
                        >
                          {/* le chien n'est pas dans la liste des chiens de l'utilisateur connecté*/}
                          {(
                            user?.user_extra_data as UserExtraData
                          )?.dogs?.findIndex(
                            (dog: Dog) =>
                              dog.id === (walkSubscription.dog as Dog).id
                          ) === -1
                            ? "Refuser"
                            : "Désinscrire"}
                        </Button>
                      </Grid>
                    )}
                </>
              </Grid>
            </Grid>
          </Grid>
        );
      })}
    </>
  );
};

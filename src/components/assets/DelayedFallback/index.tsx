import React, { useState, useEffect } from 'react'
import {useCustomBackdrop} from "../../../lib/CustomBackdrop";
const DelayedFallback = () => {
    const [show, setShow] = useState(false)

    const { displayBackdrop, closeBackdrop} = useCustomBackdrop();
    useEffect(() => {
        let timeout = setTimeout(() => {
            displayBackdrop();
            setShow(true)
        }, 300)
        return () => {
            closeBackdrop();
            clearTimeout(timeout)
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    return (
        <>
            {show && <h3>Loading ...</h3>}
        </>
    )
}
export default DelayedFallback
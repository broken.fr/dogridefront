import { useCallback } from "react";
import { FormikHelpers, FormikValues } from "formik";
import { useTranslation } from "react-i18next";
import { useNavigate } from "react-router-dom";
import { useUserExtraDataContext } from "../lib/UserExtraDataContext";
import { AxiosConnection } from "../lib/AxiosConnection";
import { Dog } from "../interfaces/dog";
import { Breed } from "../interfaces/breed";
import { useToast } from "./useToast";
import { MediaObject } from "../interfaces/mediaobject";

export const useDogForm = (dog?: Dog) => {
  const { getQuery, postQuery, postMultipart, patchQuery, deleteQuery } =
    AxiosConnection();
  const { t } = useTranslation();
  const { userExtraData, setUserExtraData } = useUserExtraDataContext();
  const navigate = useNavigate();
  const generateToast = useToast();

  const getBreed = useCallback(
    async (values: FormikValues) => {
      if (typeof values.breed !== "object") {
        try {
          const response = await getQuery("breeds?name=" + values.breed);
          const breeds = response.data["hydra:member"];
          let breed;
          if (breeds.length !== 0) {
            breeds.forEach((item: Breed) => {
              if (
                item.name.trim().toUpperCase() ===
                values.breed.trim().toUpperCase()
              ) {
                breed = {
                  name: item.name,
                  "@id": item["@id"],
                  id: item.id,
                };
              }
            });
          }
          return breed ?? { name: values.breed };
        } catch (e) {
          generateToast(t("dog.edit.error"), "post-get-breed-error");
        }
      }
    },
    [getQuery, t, generateToast]
  );

  const handleBreedViolations = useCallback(
    (violations: any[], formikProps: FormikHelpers<FormikValues>) => {
      violations.forEach((item: any) => {
        if (item?.propertyPath === "breed.name") {
          formikProps.setErrors({
            breed:
              "Chaque mot composant le nom de la race doit être prééxistant dans nos données.",
          });

          generateToast(t("dog.edit.breed_error"), "submit-dog-error");
        }
      });
    },
    [t, generateToast]
  );

  const submitDog = useCallback(
    async (values: FormikValues, dog: Dog | undefined) => {
      const postData: FormikValues = {
        ...values,
        user_extra_data: userExtraData?.["@id"],
      };
      postData.breed = (values.breed as Breed)?.["@id"] ?? values.breed;
      const result = dog
        ? await patchQuery("dogs/" + dog.id, postData)
        : await postQuery("dogs", postData);
      const message = dog ? t("dog.edit.success") : t("dog.add.success");

      userExtraData?.dogs?.push(result.data);
      setUserExtraData(userExtraData);

      generateToast(message, "submit-dog-success", "success");
      navigate("/dog/profile/" + result.data.id);
    },
    [
      userExtraData,
      patchQuery,
      postQuery,
      t,
      setUserExtraData,
      generateToast,
      navigate,
    ]
  );

  const submitImage = useCallback(
    async (
      values: FormikValues,
      dog: Dog | undefined,
      formikProps: FormikHelpers<FormikValues>
    ) => {
      const isNewImage = values.image.startsWith("data:image");

      if (!isNewImage && dog) {
        try {
          delete values.image;
          await submitDog(values, dog);
        } catch (e: any) {
          handleBreedViolations(e?.response?.data?.violations, formikProps);
          formikProps.setSubmitting(false);
          generateToast(t("dog.edit.error"), "submit-dog-error");
        }
        return;
      }

      const formData = new FormData();
      const file = await fetch(values.image)
        .then((res) => res.blob())
        .then((blob) => {
          return new File([blob], "dog.png", { type: "image/png" });
        });

      isNewImage && formData.append("file", file);
      try {
        const response = await postMultipart("media_objects", formData);
        values.image = response.data["@id"];
        const oldImage = dog?.image as MediaObject as MediaObject;

        try {
          await submitDog(values, dog);
        } catch (e: any) {
          handleBreedViolations(e?.response?.data?.violations, formikProps);

          formikProps.setSubmitting(false);
          generateToast(t("dog.edit.error"), "submit-dog-error");
        }
        if (oldImage?.["@id"]) {
          try {
            await deleteQuery(oldImage["@id"].replace("/api/", ""));
          } catch {
            formikProps.setSubmitting(false);
            generateToast(
              t("dog.edit.delete_image_error"),
              "delete-image-error"
            );
          }
        }
      } catch {
        generateToast(t("dog.edit.image_error"), "submit-dog-error");
        formikProps.setSubmitting(false);
      }
    },
    [
      submitDog,
      handleBreedViolations,
      generateToast,
      t,
      postMultipart,
      deleteQuery,
    ]
  );

  return useCallback(
    async (values: FormikValues, formikProps: FormikHelpers<FormikValues>) => {
      values.breed = await getBreed(values);
      await submitImage(values, dog, formikProps);
      // formikProps.setSubmitting(false)
      // console.log(values)
    },
    [dog, getBreed, submitImage]
  );
};

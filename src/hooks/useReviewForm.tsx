import { useCallback } from "react";
import { FormikValues } from "formik";
import { useTranslation } from "react-i18next";
import { useNavigate } from "react-router-dom";
import { AxiosConnection } from "../lib/AxiosConnection";
import { useToast } from "./useToast";
import { Walk } from "../interfaces/walk";
import { omit } from "lodash";
import { Review } from "../interfaces/review";
import { Dog } from "../interfaces/dog";
import { useUserExtraDataContext } from "../lib/UserExtraDataContext";
import { UserExtraData } from "../interfaces/userextradata";
import axios from "axios";

export const useReviewForm = (walk?: Walk) => {
  const { postQuery } = AxiosConnection();
  const { t } = useTranslation();
  const navigate = useNavigate();
  const toast = useToast();
  const { userExtraData } = useUserExtraDataContext();

  return useCallback(
    async (values: FormikValues) => {
      const dogs = omit(values, ["walk"]);
      let reviews: Review[] = [];
      Object.entries(dogs).forEach(([key, value]) => {
        const walkSubscription = walk?.walk_subscriptions?.find(
          (walkSubscription) => (walkSubscription.dog as Dog)?.id === key
        );
        reviews.push({
          score: Number(value),
          origin_entity_class: "App\\Entity\\Walk",
          origin_entity_id: walk?.id,
          reviewed_entity_class: "App\\Entity\\Dog",
          reviewed_entity_id: key,
          related_entity_class: "App\\Entity\\UserExtraData",
          related_entity_id: (walkSubscription?.dog as Dog)?.user_extra_data
            ?.id,
          type: "dog_review",
          reviewer: userExtraData?.["@id"],
        });
      });
      reviews.push({
        score: Number(values.walk),
        origin_entity_class: "App\\Entity\\Walk",
        origin_entity_id: walk?.id,
        reviewed_entity_class: "App\\Entity\\Walk",
        reviewed_entity_id: walk?.id,
        related_entity_class: "App\\Entity\\UserExtraData",
        related_entity_id: (walk?.owner as UserExtraData)?.id,
        type: "dog_review",
        reviewer: userExtraData?.["@id"],
      });
      axios
        .all(reviews.map((review) => postQuery("reviews", review)))
        .then(() => {
          toast(t("review.create.success"), "post-review-success", "success");
          navigate("/");
        })
        .catch(() => {
          toast(t("review.create.error"), "post-review-error");
        });
    },
    [
      navigate,
      postQuery,
      t,
      toast,
      userExtraData,
      walk?.id,
      walk?.owner,
      walk?.walk_subscriptions,
    ]
  );
};

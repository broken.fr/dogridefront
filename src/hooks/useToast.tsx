import { useCallback } from "react";
import { toast } from "react-toastify";

export const useToast = () => {
  return useCallback(
    (
      error: string,
      id: string,
      type: "error" | "success" | "warning" = "error"
    ) => {
      toast[type](error, {
        position: "bottom-right",
        autoClose: 3000,
        hideProgressBar: true,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        toastId: id,
      });
    },
    []
  );
};

import { useCallback } from "react";
import { AxiosConnection } from "../lib/AxiosConnection";
import { useTranslation } from "react-i18next";
import { FormikValues } from "formik";
import { useUserContext } from "../stores/casl/store";
import { useUserExtraDataContext } from "../lib/UserExtraDataContext";
import { useAuth0 } from "@auth0/auth0-react";
import { useToast } from "./useToast";
import { pick } from "lodash";
import { useNavigate } from "react-router-dom";

export const useUserExtraDataForm = () => {
  const { postQuery, patchQuery } = AxiosConnection();
  const { t } = useTranslation();
  const user = useUserContext();
  const { userExtraData, setUserExtraData } = useUserExtraDataContext();
  const toast = useToast();
  const navigate = useNavigate();

  const { getAccessTokenSilently } = useAuth0();
  console.log(userExtraData)
  return useCallback(
    async (values: FormikValues) => {
      const postData = {
        ...pick(values, [
          "positive_way",
          "natural_way",
          "traditional_way",
          "travel_area",
          "resume",
        ]),
        user: "/api/users/" + user?.user_id,
      };

      try {
        let response;

        // Créer ou mettre à jour les données utilisateur
        if (!userExtraData) {
          response = await postQuery("user_extra_datas", postData);
        } else {
          response = await patchQuery(
            "user_extra_datas/" + userExtraData.id,
            postData
          );
        }

        toast(
          t("user_extra_data.success"),
          "submit-user-infos-success",
          "success"
        );

        if (!userExtraData) {
          await getAccessTokenSilently({ ignoreCache: true });
        }

        setUserExtraData(response.data);

        if (userExtraData?.dogs?.length == 0) {
          return navigate("/dog/create");
        }
        navigate("/user/profile");
      } catch (e) {
        toast(t("user_extra_data.error"), "submit-user-infos-error");
      }
    },
    [
      user?.user_id,
      userExtraData,
      toast,
      t,
      setUserExtraData,
      navigate,
      postQuery,
      patchQuery,
      getAccessTokenSilently,
    ]
  );
};

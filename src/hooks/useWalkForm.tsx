import { useCallback } from "react";
import { FormikHelpers, FormikValues } from "formik";
import { useTranslation } from "react-i18next";
import { useNavigate } from "react-router-dom";
import { useUserExtraDataContext } from "../lib/UserExtraDataContext";
import { AxiosConnection } from "../lib/AxiosConnection";
import { useToast } from "./useToast";
import { Walk } from "../interfaces/walk";
import { pick } from "lodash";

export const useWalkForm = (walk?: Walk) => {
  const { postQuery, patchQuery } = AxiosConnection();
  const { t } = useTranslation();
  const { userExtraData } = useUserExtraDataContext();
  const navigate = useNavigate();
  const generateToast = useToast();

  return useCallback(
    async (values: FormikValues, formikProps: FormikHelpers<FormikValues>) => {
      values.longitude = values.coordinates[1];
      values.latitude = values.coordinates[0];
      if (!walk) {
        values.walk_subscriptions = values.walk_subscriptions.map((sub: any) =>
          pick(sub, ["dog", "id", "@id", "accepted", "walk"])
        );
      } else {
        delete values.walk_subscriptions;
      }
      const postData: Walk = {
        ...pick(values, [
          "description",
          "longitude",
          "latitude",
          "walk_type",
          "participants_max",
          "walk_duration",
          "walk_date",
          "rules_acceptance",
          "walk_subscriptions",
        ]),
        owner: "/api/user_extra_datas/" + userExtraData?.id,
      };
      try {
        let response;
        if (walk) {
          response = await patchQuery("walks/" + walk.id, postData);
        } else {
          response = await postQuery("walks", postData);
        }
        navigate("/walk/view/" + response.data.id, { replace: true });
      } catch {
        generateToast(
          t(`walk.${walk ? "edit" : "create"}.error`),
          "submit-walk-error"
        );
      }
    },
    [generateToast, navigate, patchQuery, postQuery, t, userExtraData?.id, walk]
  );
};

import { useCallback } from "react";
import { FormikValues } from "formik";
import { useTranslation } from "react-i18next";
import { useNavigate } from "react-router-dom";
import { AxiosConnection } from "../lib/AxiosConnection";
import { useToast } from "./useToast";
import { Walk } from "../interfaces/walk";
import { WalkSubscription } from "../interfaces/walksubscription";
import { pick } from "lodash";
import axios from "axios";

export const useWalkSubscriptionForm = (walk?: Walk) => {
  const { postQuery } = AxiosConnection();
  const { t } = useTranslation();
  const navigate = useNavigate();
  const toast = useToast();

  return useCallback(
    async (values: FormikValues) => {
      const walkSubscriptions: WalkSubscription[] =
        values.walk_subscriptions.map((sub: any) => {
          return {
            ...pick(sub, ["dog", "id", "@id", "accepted"]),
            walk: "/api/walks/" + walk?.id,
          };
        });
      console.log(walkSubscriptions);

      axios
        .all(
          walkSubscriptions.map((walkSubscription) =>
            postQuery("walk_subscriptions", walkSubscription)
          )
        )
        .then(() => {
          toast(
            t("walk_subscription.create.success"),
            "post-walk-subscription-success",
            "success"
          );
          navigate("/walk/view/" + walk?.id, { replace: true });
        })
        .catch(() => {
          toast(
            t("walk_subscription.create.error"),
            "post-walk-subscription-error"
          );
          navigate("/walk/view/" + walk?.id, { replace: true });
        });
    },
    [navigate, postQuery, t, toast, walk]
  );
};

import React, { StrictMode } from "react";
import ReactDOM from "react-dom/client";
import { BrowserRouter } from "react-router-dom";
import App from "./App";
import Auth0Provider from "./Auth0Provider";
import { AbilityContextProvider } from "./stores/casl/store";
import { UserExtraDataProvider } from "./lib/UserExtraDataContext";

// init({
//     serviceName: "DogrideFront",
//     serverUrl: Config.REACT_APP_APM_URL,
//     logLevel: "debug",
//     environment: Config.REACT_APP_APM_ENV,
//     distributedTracing: true,
//     distributedTracingOrigins: ['http://localhost:3000', 'http://doggobox.loc', 'http://doggobox.loc:8082']
// })

const root = ReactDOM.createRoot(
  document.getElementById("root") as HTMLElement
);

root.render(
  <StrictMode>
    <BrowserRouter>
      <Auth0Provider>
        <UserExtraDataProvider>
          <AbilityContextProvider>
            <App />
          </AbilityContextProvider>
        </UserExtraDataProvider>
      </Auth0Provider>
    </BrowserRouter>
  </StrictMode>
);

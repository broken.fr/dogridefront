export interface Breed {
  "@id"?: string;
  id?: string;
  name: string;
  dogs?: string[];
}

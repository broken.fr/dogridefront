import { Breed } from "./breed";
import { MediaObject } from "./mediaobject";
import { UserExtraData } from "./userextradata";
import { Moment } from "moment";

type DogCategory = "category_1" | "category_2" | "category_3";
type DogGender = "female" | "male";

export interface Dog {
  "@id"?: string;
  id?: string;
  image?: string | MediaObject;
  name: string;
  breed: string | Breed;
  user_extra_data: UserExtraData;
  sterilized: boolean;
  gender: DogGender;
  birthdate: Moment;
  category: DogCategory;
  amity_males: number;
  amity_females: number;
  amity_puppies: number;
  playfullness: number;
  sharing: number;
  runner: number;
  return_response: number;
  stop_response: number;
  adventurous_temper: boolean;
  grumpy_temper: boolean;
  peaceful_temper: boolean;
  brawler_temper: boolean;
  independent_temper: boolean;
  brutal_temper: boolean;
  mild_temper: boolean;
  shy_temper: boolean;
  sniffer: number;
  swimmer: number;
  fighter: number;
  energy_level: number;
  walk_subscriptions?: string[];
  readonly resume?: string;
}

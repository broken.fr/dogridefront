export interface MediaObject {
  "@id"?: string;
  content_url?: string;
  file_path?: string;
}

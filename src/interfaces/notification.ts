import {UserExtraData} from "./userextradata";

type NotificationMessage = "walk_subscription_accepted" | "walk_subscription_pending" | "walk_subscription_refused" | "walk_subscription_created" | "walk_finished";

export interface Notification {
  "@id"?: string;
  id?: string;
  type?: string;
  message: NotificationMessage;
  seen?: boolean;
  recipient?: UserExtraData|string;
  related_entity_class?: string;
  related_entity_id?: string;
  created_at?: Date;
  updated_at?: Date;
}

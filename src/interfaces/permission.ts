export interface Permission {
  "@id"?: string;
  id?: string;
  permission_name?: string;
  description?: string;
  resource_server_name?: string;
  resource_server_identifier?: string;
  action?: string;
  subject?: string;
}

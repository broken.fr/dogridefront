import {UserExtraData} from "./userextradata";

export interface Review {
  "@id"?: string;
  id?: string;
  type?: string;
  score: number;
  reviewer?: UserExtraData|string;
  related_entity_class?: string;
  related_entity_id?: string;
  origin_entity_class?: string;
  origin_entity_id?: string;
  reviewed_entity_class?: string;
  reviewed_entity_id?: string;
  created_at?: Date;
  updated_at?: Date;
}

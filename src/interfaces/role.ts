export interface Role {
  "@id"?: string;
  id?: string;
  name?: string;
  permissions?: any;
}

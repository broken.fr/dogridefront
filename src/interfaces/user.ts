import {UserExtraData} from "./userextradata";

export interface User {
  "@id"?: string;
  user_id?: string;
  email: string;
  picture?: string;
  email_verified?: boolean;
  name?: string;
  family_name: string;
  username: string;
  roles?: any;
  permissions?: any;
  is_current_user?: boolean;
  user_extra_data?: UserExtraData|string;
}

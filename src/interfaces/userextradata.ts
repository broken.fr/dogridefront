import {Dog} from "./dog";

export interface UserExtraData {
  "@id"?: string;
  "id"?: string;
  travel_area: number;
  positive_way: number;
  natural_way: number;
  traditional_way: number;
  resume?: string;
  dogs?: Dog[];
  user: string;
  owned_walks?: any;
}

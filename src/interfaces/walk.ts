import { WalkSubscription } from "./walksubscription";
import { UserExtraData } from "./userextradata";
import { Moment } from "moment";

type WalkType =
  | "forest_walk"
  | "mountain_walk"
  | "field_walk"
  | "town_walk"
  | "canicross"
  | "canivtt";

export interface Walk {
  "@id"?: string;
  id?: string;
  longitude?: number;
  latitude?: number;
  description?: string;
  owner?: string | UserExtraData;
  walk_subscriptions?: WalkSubscription[];
  participants_max?: number;
  walk_duration?: number;
  walk_date?: Moment | string;
  walk_type?: WalkType;
  rules_acceptance?: boolean;
}

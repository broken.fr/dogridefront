import {Dog} from "./dog";
import {Walk} from "./walk";

export interface WalkSubscription {
  "@id"?: string;
  id?: string;
  dog?: string|Dog;
  walk?: string|Walk;
  accepted?: boolean;
}

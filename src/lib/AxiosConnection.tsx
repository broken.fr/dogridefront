import {useCallback, useRef} from 'react';
import axios from 'axios';
import {useAuth0} from "@auth0/auth0-react";
import Config from '../config.json';

export const AxiosConnection = () => {

    const { getAccessTokenSilently } = useAuth0();

    const accessToken = useRef<string | null>(null);

    const initAccessToken = useCallback(async () => {
        accessToken.current = await getAccessTokenSilently();
    }, [getAccessTokenSilently]);

    const postQuery = useCallback(async (path: string, data: any): Promise<any> => {
        if (!accessToken.current) {
            await initAccessToken();
        }
        return await axios.post<any[]>(`${Config.REACT_APP_DOGGOBOX_URL}/api/${path}`, data, {
            headers: {
                Authorization: `Bearer ${accessToken.current}`,
            },
        });
    }, [initAccessToken]);

    const postMultipart = useCallback(async (path: string, data: any): Promise<any> => {
        if (!accessToken.current) {
            await initAccessToken();
        }
        return await axios.post<any[]>(`${Config.REACT_APP_DOGGOBOX_URL}/api/${path}`, data, {
            headers: {
                Authorization: `Bearer ${accessToken.current}`,
                "Content-Type": "multipart/form-data",
            },
        });
    }, [initAccessToken]);

    const patchQuery = useCallback(async (path: string, data: any): Promise<any> => {
        if (!accessToken.current) {
            await initAccessToken();
        }
        return await axios.patch<any[]>(`${Config.REACT_APP_DOGGOBOX_URL}/api/${path}`, data, {
            headers: {
                Authorization: `Bearer ${accessToken.current}`,
                'Content-Type': 'application/merge-patch+json',
            },
        });
    }, [initAccessToken]);

    const getQuery = useCallback(async (path: string): Promise<any> => {
        if (!accessToken.current) {
            await initAccessToken();
        }
        return await axios.get<any[]>(`${Config.REACT_APP_DOGGOBOX_URL}/api/${path}`, {
            headers: {
                Authorization: `Bearer ${accessToken.current}`,
            },
        });
    }, [initAccessToken]);

    const deleteQuery = useCallback(async (path: string): Promise<any> => {
        if (!accessToken.current) {
            await initAccessToken();
        }
        return await axios.delete<any[]>(`${Config.REACT_APP_DOGGOBOX_URL}/api/${path}`, {
            headers: {
                Authorization: `Bearer ${accessToken.current}`,
            },
        });
    }, [initAccessToken]);

    return {getQuery, postQuery, deleteQuery, patchQuery, postMultipart};
};

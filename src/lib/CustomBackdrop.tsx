import React, {createContext, useCallback, useContext, useState} from 'react';
import {Backdrop, CircularProgress} from "@mui/material";

const BackdropContext = createContext({displayBackdrop: () => {}, closeBackdrop: () => {}})
const CustomBackdrop: React.FC<{ children: JSX.Element }> = ({ children }) => {

    const [open, setOpen] = useState<boolean>(false);

    const displayBackdrop = useCallback(() => {
        console.info('Display backdrop');
        setOpen(true);
    }, [setOpen])

    const closeBackdrop = useCallback(() => {
        console.info('Close backdrop');
        setOpen(false);
    }, [setOpen])

    return (
        <BackdropContext.Provider value={{displayBackdrop, closeBackdrop}}>
            <Backdrop open={open} sx={{ color: '#fff', zIndex: (theme) => theme.zIndex.drawer + 1 }}>
                <CircularProgress color="inherit" />
            </Backdrop>
            {children}
        </BackdropContext.Provider>
)
};
export default CustomBackdrop;

export const useCustomBackdrop = () => {
    return useContext(BackdropContext)
}

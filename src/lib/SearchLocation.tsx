import * as React from "react";
import {useEffect, useMemo, useState} from "react";
import TextField from "@mui/material/TextField";
import Autocomplete from "@mui/material/Autocomplete";
import IconButton from '@mui/material/IconButton';
import {MyLocation} from "@mui/icons-material";
import LocationOnIcon from '@mui/icons-material/LocationOn';
import parse from 'autosuggest-highlight/parse';
import match from 'autosuggest-highlight/match';
import {Box, CircularProgress, Grid, Typography} from "@mui/material";
import {debounce, throttle} from "lodash";

type Place = {
    index: number;
    display_name: string;
    lat: string;
    lon: string;
    input: string;
    address: {
        house_number?: string;
        road?: string;
        city?: string;
        state?: string;
        country?: string;
        postcode?: string;
    }
};

type SearchLocationProps = {
    onCoordinatesChange: (coordinates: { lat: string; lon: string } | undefined) => void;
};

export default function SearchLocation({ onCoordinatesChange }: SearchLocationProps) {
    const [isLoading, setIsLoading] = useState(false);
    const [inputValue, setInputValue] = useState("");
    const [options, setOptions] = useState<Place[]>([]);
    const [coordinates, setCoordinates] = useState<{ lat: string; lon: string }>();

    const fetchPlaces = async (inputValue: string) => {
        setIsLoading(true)
        const response = await fetch(
            `https://nominatim.openstreetmap.org/search?format=json&q=${inputValue}&limit=5&addressdetails=1`
        );
        const data = await response.json();
        const places = data.map((place: any, index: number) => ({
            index: index,
            display_name: place.display_name,
            lat: place.lat,
            lon: place.lon,
            input: inputValue,
            address: {
                house_number:place.address.house_number,
                road: place.address.road,
                city: place.address.city || place.address.town || place.address.village,
                state: place.address.state,
                country: place.address.country,
                postcode: place.address.postcode
            },
        }));
        setIsLoading(false)
        setOptions(places);
    };

    const debouncedAndThrottled = useMemo(
        () => debounce(throttle(fetchPlaces, 500), 1000),
        []
    );

    const handleInputValueChange = async (value: string) => {
        setInputValue(value);
        if (value === "") {
            setOptions([]);
        } else {
            debouncedAndThrottled(value);
        }
    };

    const handleGeolocationClick = async () => {
        setIsLoading(true);
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition((position) => {
                setCoordinates({
                    lat: position.coords.latitude.toString(),
                    lon: position.coords.longitude.toString()
                });
            });
            setIsLoading(false);
        } else {
            console.log("Geolocation is not supported by this browser.");
            setIsLoading(false);
        }
    };

    useEffect(() => {
        if (coordinates) {
            localStorage.setItem("coordinates", JSON.stringify(coordinates));
            onCoordinatesChange(coordinates);
        }
    }, [coordinates, onCoordinatesChange]);

    return (
        <div>
            <Autocomplete
                value={null}
                onChange={(event: React.SyntheticEvent, value: string | Place | null) => {
                    if (value && typeof value !== "string") {
                        setCoordinates({lat: value.lat, lon: value.lon});
                    } else {
                        setCoordinates(undefined);
                    }
                }}
                inputValue={inputValue}
                onInputChange={(event, value) => handleInputValueChange(value)}
                options={options}
                getOptionLabel={(option) => {
                    if (typeof option === "object") {
                        return (option as Place)?.display_name as string
                    } else {
                        return option as string
                    }
                }}
                filterOptions={(x) => x}
                renderInput={(params) => (
                    <TextField
                        {...params}
                        label="Rechercher une localisation:"
                        variant="outlined"
                        fullWidth
                        sx={{width: '100%'}}
                        InputProps={{
                            ...params.InputProps,
                            endAdornment: (
                                <React.Fragment>
                                    {params.InputProps.endAdornment}
                                    {isLoading ? (
                                        <Box sx={{ display: "flex", alignItems: "center" }}>
                                            <CircularProgress color="inherit" size={20} />
                                        </Box>
                                    ) : (
                                        <IconButton
                                            sx={{ mr: 1 }}
                                            onClick={handleGeolocationClick}
                                            edge="end"
                                        >
                                            <MyLocation />
                                        </IconButton>
                                    )}
                                </React.Fragment>
                            )
                        }}
                    />
                )}
                freeSolo
                renderOption={(props, option: Place) => {
                    const primaryText = (option.address?.house_number ? option.address.house_number + ' ' : '') + (option.address?.road ? option.address.road + ' ' : '');
                    const matches = match(primaryText, option.input);
                    const parts = parse(
                        primaryText,
                        matches
                    );
                    return (
                        <li {...props} key={option.index}>
                            <Grid container alignItems="center">
                                <Grid item>
                                    <Box
                                        component={LocationOnIcon}
                                        sx={{color: 'text.secondary', mr: 2}}
                                    />
                                </Grid>
                                <Grid item xs>
                                    {parts.map((part: any, index: number) => {
                                            return (
                                                <span
                                                    key={index}
                                                    style={{
                                                        fontWeight: part.highlight ? 700 : 400,
                                                    }}
                                                >
                                                    {part.text}
                                                </span>
                                            )
                                        }
                                    )}

                                    <Typography variant="body2" color="text.secondary">
                                        {
                                            (option.address?.city ? option.address.city + ' ' : '')
                                            + (option.address?.postcode ? option.address.postcode + ' - ' : '')
                                            + (option.address?.state ? option.address.state + ' ' : '')
                                            + (option.address?.country ?? '')
                                        }
                                    </Typography>
                                </Grid>
                            </Grid>
                        </li>
                    );
                }}
            />
        </div>
    )
}
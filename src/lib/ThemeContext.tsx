import React, {Dispatch} from 'react';
interface IThemeContext {
    mode: 'dark' | 'light';
    setMode: Dispatch<any>;
}
export const ThemeContext = React.createContext<IThemeContext>({
    mode: 'dark',
    setMode: () => { }
})
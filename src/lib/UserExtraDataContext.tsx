import { UserExtraData } from "../interfaces/userextradata";
import {
  createContext,
  useCallback,
  useContext,
  useEffect,
  useState,
} from "react";
import { AxiosConnection } from "./AxiosConnection";
import { useAuth0 } from "@auth0/auth0-react";
import { useNavigate } from "react-router-dom";

interface UserExtraDataContextValue {
  userExtraData: UserExtraData | undefined;
  setUserExtraData: React.Dispatch<
    React.SetStateAction<UserExtraData | undefined>
  >;
  hasFetched: boolean;
}

export const UserExtraDataContext = createContext<UserExtraDataContextValue>({
  userExtraData: {} as UserExtraData,
  setUserExtraData: () => {},
  hasFetched: false,
});

export const useUserExtraDataContext = () => useContext(UserExtraDataContext);

interface UserExtraDataProviderProps {
  children: React.ReactNode;
}

export function UserExtraDataProvider({
  children,
}: UserExtraDataProviderProps) {
  const { user } = useAuth0();
  const { getQuery } = AxiosConnection();
  const navigate = useNavigate();

  const [userExtraData, setUserExtraData] = useState<
    UserExtraData | undefined
  >();
  const [hasFetched, setHasFetched] = useState(false);

  const fetchUserExtraData = useCallback(async () => {
    try {
      const res = await getQuery("users/" + user?.sub + "/user_extra_data");
      setUserExtraData(res.data);
    } catch (err) {
      console.error(err);
      setHasFetched(true);
    }
  }, [getQuery, setUserExtraData, user?.sub]);

  useEffect(() => {
    if (!userExtraData && user?.sub) {
      fetchUserExtraData();
    }
  }, [user?.sub, userExtraData, fetchUserExtraData]);

  useEffect(() => {
    // Checking if user is not loggedIn
    if (!userExtraData && hasFetched) {
      return navigate("user/profile/edit");
    }
  }, [navigate, hasFetched, userExtraData]);
  useEffect(() => {
    // Checking if user are dog
     if (userExtraData?.dogs?.length === 0) {
      return navigate("dog/create");
    }
  }, [navigate, userExtraData]);
  return (
    <UserExtraDataContext.Provider
      value={{ userExtraData, setUserExtraData, hasFetched }}
    >
      {children}
    </UserExtraDataContext.Provider>
  );
}

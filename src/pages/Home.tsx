import React from 'react';
import { useAuth0 } from "@auth0/auth0-react";
import ListWalks from "./walk/ListWalks";
import {Typography} from "@mui/material";

const Home = () => {
    const { user, isAuthenticated, isLoading } = useAuth0();
    if (isLoading) {
        return <div>Loading ...</div>;
    }
    return (
        isAuthenticated ? (
            <div>
                <Typography variant="h1" align="center" mt={3} fontSize="30px">Wouf ! Wouf ! Bienvenue dans la meute DogRide 🐶!</Typography>
                <Typography align="center" fontSize="13px">Que vous soyez un amoureux des promenades en pleine nature ou simplement à la recherche de compagnons de balade pour votre toutou, vous êtes au bon endroit !</Typography>
                <ListWalks/>
            </div>
        ) : (
            <div>Vous n'êtes pas connecté/e</div>
        )

    );
};

export default Home;
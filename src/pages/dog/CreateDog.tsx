import React from "react";
import Grid from "@mui/material/Grid";
import { Typography } from "@mui/material";
import { useTranslation } from "react-i18next";
import FormBuilder from "@elevenlabs/react-formbuilder";
import DogType from "../../components/Form/Type/DogType";
import { useDogForm } from "../../hooks/useDogForm";

const CreateDog = () => {
  const { t } = useTranslation();
  const [DogForm] = React.useState<FormBuilder>(DogType);
  const onSubmit = useDogForm();
  return (
    <>
      <Grid
        container
        my={3}
        spacing={2}
        justifyContent="center"
        alignItems="center"
      >
        <Grid item xs={12}>
          <Typography variant={"h1"}>{t("dog.add.title")} 🐶</Typography>
          <Typography>
            Laissez votre chien vivre des aventures palpitantes et rencontrer de
            nouveaux amis à quatre pattes.
          </Typography>
        </Grid>
      </Grid>
      <DogForm.Formik onSubmit={onSubmit} />
    </>
  );
};
export default CreateDog;

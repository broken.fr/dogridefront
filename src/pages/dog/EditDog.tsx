import React, { useCallback, useEffect, useRef, useState } from "react";
import { useParams } from "react-router-dom";
import { Dog } from "../../interfaces/dog";
import { AxiosConnection } from "../../lib/AxiosConnection";
import { useTranslation } from "react-i18next";
import moment from "moment";
import FormBuilder from "@elevenlabs/react-formbuilder";
import DogType from "../../components/Form/Type/DogType";
import { useCustomBackdrop } from "../../lib/CustomBackdrop";
import { useDogForm } from "../../hooks/useDogForm";
import { useToast } from "../../hooks/useToast";

const EditDog = () => {
  let { id } = useParams();
  const [dog, setDog] = useState<Dog>();
  const [DogForm, setDogForm] = React.useState<FormBuilder>(DogType);
  const { getQuery } = AxiosConnection();
  const { t } = useTranslation();
  const onSubmit = useDogForm(dog);
  const { displayBackdrop, closeBackdrop } = useCustomBackdrop();
  const generateToast = useToast();

  const getDog = useCallback(() => {
    displayBackdrop();
    getQuery("dogs/" + id)
      .then((response) => {
        response.data.birthdate = moment(response.data.birthdate);
        setDog(response.data);
      })
      .catch(() => {
        generateToast(t("dog.edit.error"), "post-review-error");
      })
      .finally(() => {
        closeBackdrop();
      });
  }, [displayBackdrop, getQuery, id, generateToast, t, closeBackdrop]);

  const getDogRef = useRef(getDog);

  useEffect(() => {
    getDogRef.current();
  }, []);

  useEffect(() => {
    setDogForm(DogType(dog));
  }, [dog]);
  return <DogForm.Formik onSubmit={onSubmit} />;
};
export default EditDog;

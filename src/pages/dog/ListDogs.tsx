import { Fab, Typography } from "@mui/material";
import CardDogs from "../../components/CardDogs";
import { Link } from "react-router-dom";
import AddIcon from "@mui/icons-material/Add";
import React, {useEffect} from "react";
import { useTranslation } from "react-i18next";
import { useUserContext } from "../../stores/casl/store";
import {useUserExtraDataContext} from "../../lib/UserExtraDataContext";
import Skeleton from "@mui/material/Skeleton";

const ListDogs = () => {
    const { t } = useTranslation();
    const user = useUserContext();
    const { userExtraData } = useUserExtraDataContext();
    if (user) {
        user.user_extra_data = userExtraData;
    }

    return (
        <>
            <Fab
                variant="extended"
                sx={{ position: "fixed", bottom: 20, right: 20 }}
                aria-label="Add"
                color="primary"
                component={Link}
                to={"/dog/create"}
            >
                <AddIcon sx={{ mr: 1 }} />
                {t("dog.add.button")}
            </Fab>

            {user && user?.user_extra_data ? <CardDogs user={user}/> : <Skeleton variant="rectangular" width={200} height={200} />}
        </>
    );
};
export default ListDogs;

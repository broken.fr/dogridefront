import React, { useCallback, useEffect, useState } from "react";
import { Link, useParams } from "react-router-dom";
import { AxiosConnection } from "../../lib/AxiosConnection";
import { useTheme } from "@mui/system";
import {
  Avatar,
  Box,
  Card,
  CardContent,
  Chip,
  Fab,
  Grid,
  Typography,
} from "@mui/material";
import { Dog } from "../../interfaces/dog";
import { useTranslation } from "react-i18next";
import { styled } from "@mui/material/styles";
import { Edit } from "@mui/icons-material";
import { DogGraph } from "../../components/DogGraph";
import Config from "../../config.json";
import { MediaObject } from "../../interfaces/mediaobject";
import Skeleton from "@mui/material/Skeleton";
import Stack from "@mui/material/Stack";

import LinearProgress, {
  linearProgressClasses,
} from "@mui/material/LinearProgress";
import { Breed } from "../../interfaces/breed";
import moment from "moment/moment";
import FemaleIcon from "@mui/icons-material/Female";
import MaleIcon from "@mui/icons-material/Male";
import PetsIcon from '@mui/icons-material/Pets';
import StarBorderIcon from '@mui/icons-material/StarBorder';
import CheckCircleIcon from '@mui/icons-material/CheckCircle';
import CancelIcon from '@mui/icons-material/Cancel';

const BorderLinearProgress = styled(LinearProgress)(() => ({
  height: 10,
  borderRadius: 5,
  [`&.${linearProgressClasses.colorPrimary}`]: {
    // backgroundColor: theme.palette.grey[theme.palette.mode === 'light' ? 200 : 800],
  },
  [`& .${linearProgressClasses.bar}`]: {
    borderRadius: 5,
    // backgroundColor: theme.palette.mode === 'light' ? '#1a90ff' : '#308fe8',
  },
}));
const ProfileDog = () => {
  let { id } = useParams();
  const [dog, setDog] = useState<Dog | undefined>(undefined);
  const { getQuery } = AxiosConnection();
  const theme = useTheme();
  const { t } = useTranslation();

  const getAgeString = (birthdate: moment.Moment): string => {
    const ageYears = moment().diff(birthdate, "years");
    const ageMonths = moment().diff(birthdate, "months") % 12;
    return ageYears > 0
        ? `${ageYears} an${ageYears > 1 ? "s" : ""}`
        : `${ageMonths} mois`;
  };

  const getDog = useCallback(() => {
    const result = getQuery("dogs/" + id);
    result.then((response) => {
      setDog(response.data);
    });
  }, [id, getQuery]);

  useEffect(() => {
    getDog();
  }, [getDog]);

  function sterilized(value : boolean | undefined,gender: string |undefined){
    if(value){
      if(gender === "male"){
        return (
            <>
              <Grid item>
                <CheckCircleIcon fontSize={"large"} color={"primary"}/>
              </Grid>
              <Grid item>
                <Typography
                    variant="subtitle2"
                    sx={{ fontWeight: "bold" }}
                >
                  Castré
                </Typography>
              </Grid>
            </>
        )
      }
      if(gender === "female"){
        return (
            <>
              <Grid item>
                <CheckCircleIcon fontSize={"large"} color={"primary"}/>
              </Grid>
              <Grid item>
                <Typography
                    variant="subtitle2"
                    sx={{ fontWeight: "bold" }}
                >
                  Stérilisée
                </Typography>
              </Grid>
            </>
        )
      }
    }
    if(!value){
      if(gender === "male"){
        return (
            <>
              <Grid item>
                <CancelIcon fontSize={"large"} color={"primary"}/>
              </Grid>
              <Grid item>
                <Typography
                    variant="subtitle2"
                    sx={{ fontWeight: "bold" }}
                >
                  Non castré
                </Typography>
              </Grid>
            </>
        )
      }
      if(gender === "female"){
        return (
            <>
              <Grid item>
                <CancelIcon fontSize={"large"} color={"primary"}/>
              </Grid>
              <Grid item>
                <Typography
                    variant="subtitle2"
                    sx={{ fontWeight: "bold" }}
                >
                  Non stérilisée
                </Typography>
              </Grid>
            </>
        )
      }
    }
  }
  function gender(sex: any) {
    if (sex === 'female') {
      return (<FemaleIcon fontSize={"large"} color={"primary"}/>);
    }
    return (<MaleIcon fontSize={"large"} color={"primary"}/>)
  }
  const getChip = (content: string) => {
    return (
        <Grid item>
          <Chip label={content} color={"primary"} />
        </Grid>
    );
  };
  const birthdate = moment(dog?.birthdate);
  const ageString = getAgeString(birthdate);
  return (
      <div>
        <Fab
            variant="extended"
            sx={{ position: "fixed", bottom: 20, right: 20 }}
            aria-label="Add"
            color="primary"
            component={Link}
            to={"/dog/edit/" + dog?.id}
        >
          <Edit sx={{ mr: 1 }} />
          {t("dog.edit.button")}
        </Fab>
        <Grid
            container
            spacing={2}
            alignItems={"center"}
            justifyContent={"space-around"}
        >
          <Grid item xs={12} md={4}>
            <Box sx={{ width: "100%", overflow: "visible" }}>
              <Card sx={{ overflow: "visible" }}>
                <Box
                    style={{
                      width: "180px",
                      height: "180px",
                      margin: "90px auto -90px auto",
                      position: "relative",
                    }}
                >

                  <Avatar
                      src={
                          Config.REACT_APP_DOGGOBOX_URL +
                          (dog?.image as MediaObject)?.content_url
                      }
                      alt="Image de profil"
                      sx={{
                        width: "100%",
                        position: "absolute",
                        top: "-90px",
                        height: "100%",
                        display: "block",
                        border: "10px solid " + theme.palette.background.default,
                      }}
                  />
                </Box>
                <CardContent>
                  {dog ? (
                      <Typography
                          align={"center"}
                          gutterBottom
                          variant="h5"
                          component="div"
                      >
                        {dog.name.charAt(0).toUpperCase() + dog.name.slice(1)}
                      </Typography>

                  ) : (
                      <Box sx={{ display: "flex", justifyContent: "center" }}>
                        <Skeleton variant="text" width={200} height={50} />
                      </Box>
                  )}
                  <Grid container spacing={2} alignItems="center">
                    <Grid item xs={12}>
                      <Typography gutterBottom variant="subtitle1" component="div">
                        {dog?.user_extra_data.resume}
                      </Typography>
                    </Grid>

                    <Grid item>
                      {gender(dog?.gender)}
                    </Grid>
                    <Grid item>
                      {dog ? (
                          <Typography
                              variant="subtitle2"
                              sx={{ fontWeight: "bold" }}
                          >
                            {t(`dog.gender_types.${dog?.gender}`)}
                          </Typography>
                      ) : (
                          <Skeleton variant="text" width={100} />
                      )}
                    </Grid>
                  </Grid>

                  <Grid container spacing={2} alignItems="center">
                    <Grid item>
                      <PetsIcon fontSize={"large"} color={"primary"}/>
                    </Grid>
                    <Grid item>
                      {dog ? (
                          <Typography
                              variant="subtitle2"
                              sx={{ fontWeight: "bold" }}
                          >
                            {(dog?.breed as Breed)?.name}
                          </Typography>
                      ) : (
                          <Skeleton variant="text" width={150} />
                      )}
                    </Grid>
                  </Grid>

                  <Grid container spacing={2} alignItems="center">
                    <Grid item>
                      <StarBorderIcon fontSize={"large"} color={"primary"}/>
                    </Grid>
                    <Grid item>
                      {dog ? (
                          <Typography
                              variant="subtitle2"
                              sx={{ fontWeight: "bold" }}
                          >
                            {ageString}
                          </Typography>
                      ) : (
                          <Skeleton variant="text" width={100} />
                      )}
                    </Grid>
                  </Grid>
                  <Grid container spacing={2} alignItems="center">
                    {sterilized(dog?.sterilized, dog?.gender)}
                  </Grid>

                  <Stack mt={2} spacing={1}>
                    <Grid
                        container
                        spacing={1}
                        alignItems={"center"}
                        justifyContent={"center"}
                    >
                      {dog?.adventurous_temper &&
                          getChip(t("dog.adventurous_temper"))}
                      {dog?.grumpy_temper && getChip(t("dog.grumpy_temper"))}
                      {dog?.peaceful_temper && getChip(t("dog.peaceful_temper"))}
                      {dog?.brawler_temper && getChip(t("dog.brawler_temper"))}
                      {dog?.independent_temper &&
                          getChip(t("dog.independent_temper"))}
                      {dog?.brutal_temper && getChip(t("dog.brutal_temper"))}
                      {dog?.mild_temper && getChip(t("dog.mild_temper"))}
                      {dog?.shy_temper && getChip(t("dog.shy_temper"))}
                    </Grid>
                  </Stack>
                </CardContent>
              </Card>
            </Box>
          </Grid>
          <Grid item xs={12} md={6}>
            <Box sx={{ width: "100%", overflow: "visible" }}>
              <Card>
                <CardContent>
                  <Typography gutterBottom variant="subtitle1" component="div">
                    {t("dog.amity_males")}
                  </Typography>
                  <BorderLinearProgress
                      {...(dog
                          ? {
                            value: dog.amity_males * 20,
                            variant: "determinate",
                          }
                          : {})}
                      color={"inherit"}
                  />
                  <Typography
                      gutterBottom
                      mt={2}
                      variant="subtitle1"
                      component="div"
                  >
                    {t("dog.amity_females")}
                  </Typography>
                  <BorderLinearProgress
                      {...(dog
                          ? {
                            value: dog.amity_females * 20,
                            variant: "determinate",
                          }
                          : {})}
                      color={"info"}
                  />
                  <Typography
                      gutterBottom
                      mt={2}
                      variant="subtitle1"
                      component="div"
                  >
                    {t("dog.amity_puppies")}
                  </Typography>
                  <BorderLinearProgress
                      {...(dog
                          ? {
                            value: dog.amity_puppies * 20,
                            variant: "determinate",
                          }
                          : {})}
                      color={"secondary"}
                  />
                  <Typography
                      gutterBottom
                      mt={2}
                      variant="subtitle1"
                      component="div"
                  >
                    {t("dog.energy_level")}
                  </Typography>
                  <BorderLinearProgress
                      {...(dog
                          ? {
                            value: dog.energy_level * 20,
                            variant: "determinate",
                          }
                          : {})}
                      color={"success"}
                  />
                  <Typography
                      gutterBottom
                      mt={2}
                      variant="subtitle1"
                      component="div"
                  >
                    {t("dog.return_response")}
                  </Typography>
                  <BorderLinearProgress
                      {...(dog
                          ? {
                            value: dog.return_response * 20,
                            variant: "determinate",
                          }
                          : {})}
                      color={"primary"}
                  />
                  <Typography
                      gutterBottom
                      mt={2}
                      variant="subtitle1"
                      component="div"
                  >
                    {t("dog.stop_response")}
                  </Typography>
                  <BorderLinearProgress
                      {...(dog
                          ? {
                            value: dog.stop_response * 20,
                            variant: "determinate",
                          }
                          : {})}
                      color={"warning"}
                  />
                  <Typography
                      gutterBottom
                      mt={2}
                      variant="subtitle1"
                      component="div"
                  >
                    {t("dog.sharing")}
                  </Typography>
                  <BorderLinearProgress
                      {...(dog
                          ? {
                            value: dog.sharing * 20,
                            variant: "determinate",
                          }
                          : {})}
                      color={"error"}
                  />
                </CardContent>
              </Card>
            </Box>
          </Grid>
          <Grid item xs={12}>
            <Box
                sx={{
                  width: "100%",
                  height: "40vw",
                  typography: "body1",
                  display: "flex",
                  justifyContent: "center",
                }}
                mb={10}
            >
              {dog ? (
                  <DogGraph dog={dog} />
              ) : (
                  <Skeleton
                      variant="rectangular"
                      sx={{ margin: "100px 0" }}
                      width={"50%"}
                      height={"100%"}
                  />
              )}
            </Box>
          </Grid>
        </Grid>
      </div>
  );
};
export default ProfileDog;

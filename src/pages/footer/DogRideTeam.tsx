import React from "react";
import {Typography} from "@mui/material";

const DogRideTeam = () => {

    return (
        <>
            <Typography variant={"h1"}>La Meute de Choc de DogRide !</Typography>

            <Typography>
                Bienvenue dans l'univers de DogRide ! Notre application a été créée avec passion par une équipe talentueuse et dévouée, déterminée à offrir une expérience unique aux amoureux des chiens. Laissez-nous vous présenter les personnes qui ont donné vie à DogRide :
            </Typography>
            <Typography variant={"h3"} fontSize={"20px"} mb={1} mt={4}>
                🐱 Valentin - CTO (Chief Technology Officer) :
            </Typography>
            <Typography textAlign={"center"} ml={5} mr={5}>
                Valentin est notre expert technique, responsable du développement et de la mise en œuvre de notre plateforme. Grâce à son expertise en matière de technologies et à son dévouement à créer une expérience fluide et intuitive, il garantit que DogRide fonctionne à la perfection.
            </Typography>
            <Typography variant={"h3"} fontSize={"20px"} mb={1} mt={4}>
                🦄 Vera - Chef de Projet + Développeuse :
            </Typography>
            <Typography textAlign={"center"} ml={5} mr={5}>
                Vera est à la fois notre chef de projet et développeuse. Elle coordonne les différentes étapes du processus de développement et veille à ce que chaque détail soit pris en compte. Elle apporte également sa passion pour la programmation et sa créativité pour créer une application attrayante et performante.
            </Typography>
            <Typography variant={"h3"} fontSize={"20px"} mb={1} mt={4}>
                🐶 Laetitia - Community Manager :
            </Typography>
            <Typography textAlign={"center"} ml={5} mr={5}>
                Laetitia est notre Community Manager, chargée de cultiver une communauté engagée et de maintenir une communication fluide avec nos utilisateurs. Grâce à sa passion pour les chiens et son expertise dans la gestion des réseaux sociaux, elle assure que chaque membre de DogRide se sente écouté et soutenu.
            </Typography>
            <Typography variant={"h3"} fontSize={"20px"} mb={1} mt={4}>
                👨‍💻 Matthias - Développeur :
            </Typography>
            <Typography textAlign={"center"} ml={5} mr={5}>
                Matthias est notre talentueux développeur, apportant son expertise technique et sa vision novatrice à notre équipe. Il travaille en étroite collaboration avec Valentin pour créer des fonctionnalités uniques et améliorer continuellement l'expérience utilisateur de DogRide.
            </Typography>
            <Typography variant={"h3"} fontSize={"20px"} mb={1} mt={4}>
                ⭐ Julia Debeul - Éducatrice Canine en Positif :
            </Typography>
            <Typography textAlign={"center"} ml={5} mr={5}>
                Nous tenons également à remercier chaleureusement Julia Debeul, notre précieuse éducatrice canine en positif. Julia a apporté son expertise et ses connaissances approfondies en matière de comportement canin pour nous aider à développer une approche respectueuse et bienveillante dans l'élaboration de DogRide.
            </Typography>
            <Typography mt={5} textAlign={"center"}>
                Ensemble, nous formons une équipe dynamique, passionnée par les chiens et déterminée à offrir une plateforme exceptionnelle pour des balades canines mémorables. Nous sommes ravis de vous accueillir sur DogRide et espérons que vous vivrez une expérience inoubliable en notre compagnie !
            </Typography>
        </>
    );
};
export default DogRideTeam;

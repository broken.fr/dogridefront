import {Typography} from "@mui/material";
import React from "react";

const LegalNotice = () => {

    return (
        <>
            <Typography variant={"h1"} mb={2}>Mentions légales</Typography>
            <Typography variant={"h3"} fontSize={"20px"} mb={1} mt={2}>
                Collecte et stockage des données :
            </Typography>
                <Typography>
                DogRide garantit qu'aucun traçage ni aucun produit de Google, Amazon ou Meta ne sont utilisés sur notre plateforme. Nous respectons votre vie privée et nous nous engageons à protéger vos informations personnelles. Les seules informations stockées dans notre base de données sont celles fournies par nos membres lors de leur inscription.
                </Typography>
            <Typography variant={"h3"} fontSize={"20px"} mb={1} mt={2}>
                Utilisation des données :
            </Typography>
            <Typography>
                Les informations collectées sont utilisées dans le but de faciliter la communication entre les membres et de proposer des balades de chiens en groupe. Vos données personnelles ne seront en aucun cas vendues, louées ou partagées avec des tiers sans votre consentement préalable.
            </Typography>
            <Typography variant={"h3"} fontSize={"20px"} mb={1} mt={2}>
                Demande de suppression des données :
            </Typography>
            <Typography>
                Chaque membre a le droit de demander la suppression de ses informations personnelles de notre base de données. Pour effectuer une telle demande, veuillez nous contacter par e-mail à l'adresse suivante : webmaster@dogride.fr. Nous traiterons votre demande dans les meilleurs délais.
            </Typography>
            <Typography variant={"h3"} fontSize={"20px"} mb={1} mt={2}>
                Sécurité des données :
            </Typography>
            <Typography>
                Nous mettons en place des mesures de sécurité appropriées pour protéger vos données contre tout accès non autorisé, toute divulgation, toute altération ou toute destruction. Cependant, veuillez noter que malgré nos efforts, aucune méthode de transmission de données sur Internet ou de stockage électronique n'est totalement sécurisée. Par conséquent, nous ne pouvons garantir une sécurité absolue de vos informations.
            </Typography>
            <Typography variant={"h3"} fontSize={"20px"} mb={1} mt={2}>
                Cookies :
            </Typography>
            <Typography>
                Nous n'utilisons pas de cookies sur notre site pour collecter des informations personnelles.
            </Typography>
            <Typography variant={"h3"} fontSize={"20px"} mb={1} mt={2}>
                Liens externes :
            </Typography>
            <Typography>
                Notre site peut contenir des liens vers des sites externes. Nous ne sommes pas responsables des pratiques de confidentialité ou du contenu de ces sites. Nous vous encourageons à lire les politiques de confidentialité de ces sites avant de fournir vos informations personnelles.
            </Typography>
            <Typography mt={4}>
                Si vous avez des questions supplémentaires concernant nos pratiques de confidentialité ou nos mentions légales, veuillez nous contacter à l'adresse e-mail webmaster@dogride.fr
            </Typography>
        </>
    );
};
export default LegalNotice;

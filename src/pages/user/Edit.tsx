import React, { useEffect } from "react";
import FormBuilder from "@elevenlabs/react-formbuilder";
import UserExtraDataType from "../../components/Form/Type/UserExtraDataType";
import { useUserExtraDataForm } from "../../hooks/useUserExtraDataForm";
import { useUserExtraDataContext } from "../../lib/UserExtraDataContext";
import {Typography} from "@mui/material";

const Edit = () => {
  const [UserExtraDataForm, setUserExtraDataForm] =
    React.useState<FormBuilder>(UserExtraDataType);
  const onSubmit = useUserExtraDataForm();
  const { userExtraData } = useUserExtraDataContext();
  useEffect(() => {
    setUserExtraDataForm(UserExtraDataType(userExtraData));
  }, [userExtraData]);
  return (
      <>
        <Typography variant={'h1'} fontSize={'30px'}>Prêt à vivre des aventures canines mémorables ?</Typography>
        <Typography>Ensemble, créons des souvenirs inoubliables à chaque pas que nous faisons avec nos chiens.</Typography>
        <UserExtraDataForm.Formik onSubmit={onSubmit} />
        </>
        );
};
export default Edit;

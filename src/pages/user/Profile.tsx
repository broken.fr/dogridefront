import React, {useEffect} from "react";
import { useUserExtraDataContext } from "../../lib/UserExtraDataContext";
import { useUserContext } from "../../stores/casl/store";
import CardDogs from "../../components/CardDogs";
import Skeleton from "@mui/material/Skeleton";
import UserInfo from "../../components/User/UserInfo";
import {Fab} from "@mui/material";
import {Link} from "react-router-dom";
import {Edit} from "@mui/icons-material";
import { useTranslation } from "react-i18next";
import UserExtraDataType from "../../components/Form/Type/UserExtraDataType";

const Profile = () => {
  const { t } = useTranslation();
  const user = useUserContext();
  const { userExtraData } = useUserExtraDataContext();
  if (user) {
    user.user_extra_data = userExtraData;
  }

  if (!userExtraData) return <div></div>;

  return (
      <>

          <Fab
              variant="extended"
              sx={{ position: "fixed", bottom: 20, right: 20 }}
              aria-label="Add"
              color="primary"
              component={Link}
              to={"/user/profile/edit"}
          >
            <Edit sx={{ mr: 1 }} />
            {t("profile.edit.button")}
          </Fab>

          {user ? <UserInfo user={user}/> : <Skeleton variant="rectangular" width={200} height={200} />}
        {user ? <CardDogs user={user}/> : <Skeleton variant="rectangular" width={200} height={200} />}

      </>
  );
};

export default Profile;

import {useParams} from "react-router-dom";
import React, {useCallback, useEffect, useState} from "react";
import {AxiosConnection} from "../../lib/AxiosConnection";
import {User} from "../../interfaces/user";
import UserInfo from "../../components/User/UserInfo";
import Skeleton from "@mui/material/Skeleton";
import CardDogs from "../../components/CardDogs";


const ProfileById = () => {
    let { id } = useParams();
    const [user, setUser ] = useState<User | undefined>(undefined)
    const { getQuery } = AxiosConnection();

    const getUser = useCallback(() => {
        const result = getQuery("users/" + id );
        result.then((response) => {
            setUser(response.data);
        });
    }, [id, getQuery]);

    useEffect(() => {
        getUser();
    }, [getUser]);
console.log(user)

    return (
        <>
            {user ? <UserInfo user={user}/> : <Skeleton variant="rectangular" width={200} height={200} />}
            {user ? <CardDogs user={user}/> : <Skeleton variant="rectangular" width={200} height={200} />}

        </>
    );
};
export default ProfileById;
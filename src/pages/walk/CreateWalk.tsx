import React, { useEffect, useState } from "react";
import FormBuilder from "@elevenlabs/react-formbuilder";
import WalkType from "../../components/Form/Type/WalkType";
import { useUserExtraDataContext } from "../../lib/UserExtraDataContext";
import { useWalkForm } from "../../hooks/useWalkForm";
import { WalkSubscription } from "../../interfaces/walksubscription";

const CreateWalk = () => {
  const [WalkForm, setWalkForm] = React.useState<FormBuilder>(WalkType);
  const { userExtraData } = useUserExtraDataContext();
  const [selectableDogs, setSelectableDogs] = useState<WalkSubscription[]>();
  const onSubmit = useWalkForm();

  useEffect(() => {
    setSelectableDogs(
      userExtraData?.dogs?.map((dog: any) => ({
        dog: dog?.["@id"],
        label: dog.name,
        accepted: true,
      }))
    );
  }, [userExtraData]);

  useEffect(() => {
    setWalkForm(WalkType(selectableDogs));
  }, [selectableDogs]);
  return (
    <>
      <WalkForm.Formik onSubmit={onSubmit} />
    </>
  );
};
export default CreateWalk;

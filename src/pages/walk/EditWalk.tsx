import React, { useCallback, useEffect, useRef, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { Walk } from "../../interfaces/walk";
import { AxiosConnection } from "../../lib/AxiosConnection";
import moment from "moment/moment";
import { useToast } from "../../hooks/useToast";
import { useTranslation } from "react-i18next";
import { useCustomBackdrop } from "../../lib/CustomBackdrop";
import FormBuilder from "@elevenlabs/react-formbuilder";
import WalkType from "../../components/Form/Type/WalkType";
import { WalkSubscription } from "../../interfaces/walksubscription";
import { useUserExtraDataContext } from "../../lib/UserExtraDataContext";
import { useWalkForm } from "../../hooks/useWalkForm";

const EditWalk = () => {
  const { id } = useParams<{ id: string }>();
  const [walk, setWalk] = useState<Walk>();
  const { userExtraData } = useUserExtraDataContext();
  const [WalkForm, setWalkForm] = React.useState<FormBuilder>(WalkType);
  const [selectableDogs, setSelectableDogs] = useState<WalkSubscription[]>();
  const { getQuery } = AxiosConnection();
  const toast = useToast();
  const { t } = useTranslation();
  const { displayBackdrop, closeBackdrop } = useCustomBackdrop();
  const onSubmit = useWalkForm(walk);
  const navigate = useNavigate();

  const fetchWalk = useCallback(async () => {
    try {
      displayBackdrop();
      const response = await getQuery(`walks/${id}`);
      const walk: Walk = response.data;
      walk.walk_date = moment(walk.walk_date);
      setWalk(walk);
      if (walk.walk_date.isBefore(moment())) {
        toast(t("walk.walk_is_finished_error"), "walk-date-error");
        navigate("/");
      }
    } catch {
      toast(t("walk.edit.get_error"), "get-walk-infos-error");
    } finally {
      closeBackdrop();
    }
  }, [displayBackdrop, getQuery, id, toast, t, navigate, closeBackdrop]);

  const fetchWalkRef = useRef(fetchWalk);

  useEffect(() => {
    fetchWalkRef.current();
  }, [fetchWalk]);

  useEffect(() => {
    setSelectableDogs(
      userExtraData?.dogs?.map((dog: any) => ({
        dog: dog?.["@id"],
        label: dog.name,
        accepted: true,
      }))
    );
  }, [userExtraData]);

  useEffect(() => {
    if (selectableDogs && walk) {
      setWalkForm(WalkType(selectableDogs, walk));
    }
  }, [selectableDogs, walk]);
  return <WalkForm.Formik onSubmit={onSubmit} />;
  // return <WalkForm walk={walk} />;
};
export default EditWalk;

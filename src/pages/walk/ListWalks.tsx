import { AxiosConnection } from "../../lib/AxiosConnection";
import React, { useCallback, useEffect, useRef, useState } from "react";
import { Walk } from "../../interfaces/walk";
import { MapContainer, Marker, TileLayer } from "react-leaflet";
import L, { LatLngExpression } from "leaflet";
import "leaflet/dist/leaflet.css";
import {
  Button,
  Card,
  CardContent,
  Fab,
  Grid,
  Typography,
} from "@mui/material";
import AddIcon from "@mui/icons-material/Add";
import { Link } from "react-router-dom";
import SearchLocation from "../../lib/SearchLocation";
import moment from "moment";
import CardActions from "@mui/material/CardActions";
import CalendarTodayIcon from "@mui/icons-material/CalendarToday";
import QueryBuilderIcon from "@mui/icons-material/QueryBuilder";
import PeopleIcon from "@mui/icons-material/People";
import Loader from "../../lib/Loader";
import {useTranslation} from "react-i18next";
import WalkType from "../../components/Form/Type/WalkType";
import WalkSubscriptionType from "../../components/Form/Type/WalkSubscriptionType";

let DefaultIcon = L.icon({
  iconUrl: require("leaflet/dist/images/marker-icon.png"),
  shadowUrl: require("leaflet/dist/images/marker-shadow.png"),
  iconSize: [24, 40],
  iconAnchor: [12, 38],
});

L.Marker.prototype.options.icon = DefaultIcon;

const ListWalks = () => {
  const [walks, setWalks] = useState<Walk[] | undefined>(undefined);
  const startCoordinates:
    | {
        lat: string;
        lon: string;
      }
    | undefined = localStorage.getItem("coordinates")
    ? JSON.parse(localStorage.getItem("coordinates") || "")
    : undefined;
  const [currentCoordinates, setCurrentCoordinates] = useState<
    | {
        lat: string;
        lon: string;
      }
    | undefined
  >(startCoordinates);
  const previousCoordinates = useRef(currentCoordinates);
  const { getQuery } = AxiosConnection();
  const [page, setPage] = useState(1);
  const [loading, setLoading] = useState(false);
  const [hasMore, setHasMore] = useState(true);
  const [loadingMore, setLoadingMore] = useState(false);
  const itemsPerPage = 10;
  const {t} = useTranslation()

  const getWalks = useCallback(() => {
    setLoading(true);
    if (currentCoordinates !== previousCoordinates.current) {
      setPage(1);
      setWalks([]);
      previousCoordinates.current = currentCoordinates;
    }
    getQuery(
      "walks?nearby=" +
        currentCoordinates?.lat +
        "," +
        currentCoordinates?.lon +
        "&walk_date[after]=" +
        new Date().toISOString() +
        `&page=${page}&itemsPerPage=${itemsPerPage}&order[walk_date]=ASC`
    ).then((response) => {
      setWalks((prevWalks) => [
        ...(prevWalks || []),
        ...response.data["hydra:member"],
      ]);
      setLoading(false);
      setLoadingMore(false);
      setHasMore(
        Boolean(
          response.data?.["hydra:view"]?.["hydra:next"]?.replace("/api/", "")
        )
      );
    });
  }, [getQuery, page, currentCoordinates]);

  useEffect(() => {
    if (currentCoordinates) {
      getWalks();
    }
  }, [currentCoordinates, getWalks]);

  const loadMoreWalks = useCallback(() => {
    setLoadingMore(true);
    setPage((page) => page + 1);
  }, []);

  useEffect(() => {
    const handleScroll = () => {
      const scrollTop =
        (document.documentElement && document.documentElement.scrollTop) ||
        document.body.scrollTop;
      const scrollHeight =
        (document.documentElement && document.documentElement.scrollHeight) ||
        document.body.scrollHeight;
      const clientHeight =
        document.documentElement.clientHeight || window.innerHeight;
      const scrolledToBottom =
        Math.ceil(scrollTop + clientHeight) >= scrollHeight;
      if (scrolledToBottom && !loadingMore && hasMore) {
        loadMoreWalks();
      }
    };

    window.addEventListener("scroll", handleScroll);
    return () => window.removeEventListener("scroll", handleScroll);
  }, [loadMoreWalks, loadingMore, hasMore]);

  return (
    <>
      <Typography mt={5} mb={1}>Où allons-nous nous balader aujourd'hui ?</Typography>
      <SearchLocation
        onCoordinatesChange={(coordinates) => {
          setCurrentCoordinates(coordinates);
        }}
      />
      <Grid
        container
        spacing={4}
        sx={{ marginTop: "20px", marginBottom: "40px" }}
      >
        <>
          {walks?.map((walk: Walk, index) => {
            const numberOfAcceptedParticipants =
              walk.walk_subscriptions?.reduce(
                (acceptedTotal, walk_subscriptions) =>
                  walk_subscriptions.accepted === true
                    ? ++acceptedTotal
                    : acceptedTotal,
                0
              );

            const position = [
              walk.latitude ?? 0,
              walk.longitude ?? 0,
            ] as LatLngExpression;

            const duration = moment.duration(walk.walk_duration, "minutes");
            const hours = Math.floor(duration.asHours());
            const minutes = duration.minutes();
            const durationString =
              hours > 0
                ? `${hours}h${minutes.toString().padStart(2, "0")}`
                : `${minutes}min`;
            return (
              <Grid item key={index} xs={12} sm={6} md={4}>
                <Card>
                  <CardContent>
                    <Grid container spacing={2}>
                      <Grid item xs={12} sm={12} textAlign={"center"}>
                        <Typography variant="h3" fontSize={"25px"} component="p" >
                          {walk?.walk_type
                              ? t(`walk.walk_type.${walk.walk_type}`)
                              : t("walk.walk_type.undefined")}
                        </Typography>
                      </Grid>
                      <Grid item xs={12} sm={12}>
                        <Typography variant="subtitle1" color="textSecondary">
                          Date de la balade
                        </Typography>
                        <div style={{ display: "flex", alignItems: "center" }}>
                          <CalendarTodayIcon
                            color="primary"
                            style={{ marginRight: "8px" }}
                          />
                          <Typography variant="body1" component="p">
                            {moment(walk.walk_date as string).format("llll")}
                            <br />
                          </Typography>
                        </div>
                      </Grid>
                      <Grid item xs={12} sm={6}>
                        <Typography variant="subtitle1" color="textSecondary">
                          Durée de la balade
                        </Typography>
                        <div style={{ display: "flex", alignItems: "center" }}>
                          <QueryBuilderIcon
                            color="primary"
                            style={{ marginRight: "8px" }}
                          />
                          <Typography variant="body1" component="p">
                            {durationString}
                          </Typography>
                        </div>
                      </Grid>
                      <Grid item xs={12} sm={6}>
                        <Typography variant="subtitle1" color="textSecondary">
                          N° de participants
                        </Typography>
                        <div style={{ display: "flex", alignItems: "center" }}>
                          <PeopleIcon
                            color="primary"
                            style={{ marginRight: "8px" }}
                          />
                          <Typography variant="body1" component="p">
                            {numberOfAcceptedParticipants} /{" "}
                            {walk.participants_max}
                          </Typography>
                        </div>
                      </Grid>
                    </Grid>
                    <div style={{ marginTop: "16px" }}>
                      <MapContainer
                        center={position}
                        zoom={13}
                        style={{ height: "200px" }}
                      >
                        <TileLayer
                          attribution='&copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors'
                          url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                        />
                        <Marker position={position} />
                      </MapContainer>
                    </div>
                  </CardContent>
                  <CardActions>
                    <Button
                      size="small"
                      color="primary"
                      component={Link}
                      to={"/walk/subscribe/" + walk.id}
                    >
                      S'inscrire
                    </Button>
                    <Button
                      size="small"
                      color="primary"
                      component={Link}
                      to={"/walk/view/" + walk.id}
                    >
                      Voir les détails
                    </Button>
                  </CardActions>
                </Card>
              </Grid>
            );
          })}
        </>
      </Grid>
      {loading ? <Loader /> : null}
      <Fab
        variant="extended"
        sx={{ position: "fixed", bottom: 20, right: 20 }}
        aria-label="Add"
        color="primary"
        component={Link}
        to="walk/create"
      >
        <AddIcon sx={{ mr: 1 }} />
        Creer une balade 🤗
      </Fab>
    </>
  );
};
export default ListWalks;

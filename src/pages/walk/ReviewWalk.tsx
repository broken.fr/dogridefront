import { useNavigate, useParams } from "react-router-dom";
import React, { useEffect, useState } from "react";
import { Walk } from "../../interfaces/walk";
import { AxiosConnection } from "../../lib/AxiosConnection";
import moment from "moment/moment";
import FormBuilder from "@elevenlabs/react-formbuilder";
import ReviewType from "../../components/Form/Type/ReviewType";
import { useReviewForm } from "../../hooks/useReviewForm";
import { useToast } from "../../hooks/useToast";
import { useTranslation } from "react-i18next";
import { useCustomBackdrop } from "../../lib/CustomBackdrop";

export const ReviewWalk = () => {
  const { id } = useParams<{ id: string }>();
  const [walk, setWalk] = useState<Walk>();
  const [ReviewForm, setReviewForm] = React.useState<FormBuilder>(ReviewType);
  const [isWalkFinished, setWalkFinished] = useState<boolean>(false);
  const { getQuery } = AxiosConnection();
  const navigate = useNavigate();
  const onSubmit = useReviewForm(walk);
  const toast = useToast();
  const { t } = useTranslation();
  const { displayBackdrop, closeBackdrop } = useCustomBackdrop();

  useEffect(() => {
    const fetchWalk = () => {
      displayBackdrop();
      getQuery(`walks/${id}`)
        .then((response) => {
          const walk = response.data;
          setWalk(walk);
        })
        .catch(() => {
          toast(t("review.create.error"), "get-walk-infos-error");
          closeBackdrop();
        });
    };
    fetchWalk();
  }, [id, getQuery, toast, t, displayBackdrop, closeBackdrop]);

  useEffect(() => {
    if (walk) {
      const walkDate = moment(walk.walk_date);
      const isWalkFinished = walkDate
        .add(walk?.walk_duration, "minutes")
        .isBefore(moment());
      setWalkFinished(isWalkFinished);
      if (!isWalkFinished) {
        toast(
          t("review.create.walk_unfinished"),
          "walk-not-finished-warning",
          "warning"
        );
        navigate("/");
      }
    }
  }, [walk, navigate, toast, t]);

  useEffect(() => {
    if (walk) {
      setReviewForm(ReviewType(walk));
      closeBackdrop();
    }
  }, [closeBackdrop, walk]);

  return (
    <>{walk && isWalkFinished && <ReviewForm.Formik onSubmit={onSubmit} />}</>
  );
};

import { useNavigate, useParams } from "react-router-dom";
import React, { useCallback, useEffect, useRef, useState } from "react";
import FormBuilder from "@elevenlabs/react-formbuilder";
import WalkSubscriptionType from "../../components/Form/Type/WalkSubscriptionType";
import { Walk } from "../../interfaces/walk";
import moment from "moment";
import { WalkSubscription } from "../../interfaces/walksubscription";
import { AxiosConnection } from "../../lib/AxiosConnection";
import { useToast } from "../../hooks/useToast";
import { useTranslation } from "react-i18next";
import { useCustomBackdrop } from "../../lib/CustomBackdrop";
import { useUserExtraDataContext } from "../../lib/UserExtraDataContext";
import { useWalkSubscriptionForm } from "../../hooks/useWalkSubscriptionForm";
import { Dog } from "../../interfaces/dog";

export const SubscribeWalk = () => {
  const { id } = useParams<{ id: string }>();
  const [walk, setWalk] = useState<Walk>();
  const { userExtraData } = useUserExtraDataContext();
  const [WalkSubscriptionForm, setWalkSubscriptionForm] =
    React.useState<FormBuilder>(WalkSubscriptionType);
  const [selectableDogs, setSelectableDogs] = useState<WalkSubscription[]>();
  const { getQuery } = AxiosConnection();
  const toast = useToast();
  const { t } = useTranslation();
  const { displayBackdrop, closeBackdrop } = useCustomBackdrop();
  const onSubmit = useWalkSubscriptionForm(walk);
  const navigate = useNavigate();

  const fetchWalk = useCallback(async () => {
    try {
      displayBackdrop();
      const response = await getQuery(`walks/${id}`);
      const walk: Walk = response.data;
      walk.walk_date = moment(walk.walk_date);
      if (walk.walk_date.isBefore(moment())) {
        toast(t("walk.walk_is_finished_error"), "walk-date-error");
        navigate("/");
      }
      setWalk(walk);
    } catch {
      toast(t("walk.edit.get_error"), "get-walk-infos-error");
    } finally {
      closeBackdrop();
    }
  }, [displayBackdrop, getQuery, id, toast, t, navigate, closeBackdrop]);

  const fetchWalkRef = useRef(fetchWalk);

  useEffect(() => {
    fetchWalkRef.current();
  }, [fetchWalk]);

  useEffect(() => {
    setSelectableDogs(
      userExtraData?.dogs?.map((dog: any) => ({
        dog: dog?.["@id"],
        label: dog.name,
        accepted: false,
      }))
    );
  }, [userExtraData]);

  useEffect(() => {
    if (selectableDogs && walk) {
      walk?.walk_subscriptions?.forEach((walkSubscription) => {
        const index = selectableDogs.findIndex(
          (dog) => dog.dog === (walkSubscription?.dog as Dog)?.["@id"]
        );
        if (index !== -1) {
          selectableDogs.splice(index, 1);
        }
      });
      setWalkSubscriptionForm(WalkSubscriptionType(selectableDogs, walk));
    }
  }, [selectableDogs, walk]);
  return <WalkSubscriptionForm.Formik onSubmit={onSubmit} />;
};

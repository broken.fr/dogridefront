import { useParams } from "react-router-dom";
import { Walk } from "../../interfaces/walk";
import React, { useEffect, useState } from "react";
import { AxiosConnection } from "../../lib/AxiosConnection";
import Loader from "../../lib/Loader";
import { toast } from "react-toastify";
import { AxiosError } from "axios";
import { MapContainer, Marker, TileLayer } from "react-leaflet";
import { LatLngExpression } from "leaflet";
import {
  Avatar,
  Button,
  Card,
  CardContent,
  CardHeader,
  Typography,
} from "@mui/material";
import Grid from "@mui/material/Grid";
import { useAuth0 } from "@auth0/auth0-react";
import { UserExtraData } from "../../interfaces/userextradata";
import { User } from "../../interfaces/user";
import { red } from "@mui/material/colors";
import { UserGraph } from "../../components/User/UserGraph";
import { WalkSubscribers } from "../../components/Walk/WalkSubscribers";
import Skeleton from "@mui/material/Skeleton";
import PetsIcon from "@mui/icons-material/Pets";
import GenderIcon from "@mui/icons-material/EmojiPeople";
import BloodtypeIcon from "@mui/icons-material/Bloodtype";
import CalendarTodayIcon from "@mui/icons-material/CalendarToday";
import moment from "moment";
import QueryBuilderIcon from "@mui/icons-material/QueryBuilder";
import PeopleIcon from "@mui/icons-material/People";
import WalkActions from "../../components/Walk/WalkActions";
import { WalkSubscription } from "../../interfaces/walksubscription";
import { useTranslation } from "react-i18next";
import { Snowshoeing } from "@mui/icons-material";

export const ViewWalk = () => {
  const { id } = useParams<{ id: string }>();
  const [walk, setWalk] = useState<Walk>();
  const [error, setError] = useState<string | null>(null);
  const [owner, setOwner] = useState<User>();
  const { getQuery } = AxiosConnection();
  const { t } = useTranslation();
  const { isAuthenticated, getAccessTokenSilently } = useAuth0();
  const updateWalkSubscriptions = (newSubscriptions: WalkSubscription[]) => {
    setWalk((prevWalk) => ({
      ...prevWalk,
      walk_subscriptions: newSubscriptions,
    }));
  };

  useEffect(() => {
    const fetchOwnerInfo = async () => {
      const token = isAuthenticated ? await getAccessTokenSilently() : null;

      if (token) {
        const ownerId = (walk?.owner as UserExtraData).user.replace(
            "/api/",
            ""
        );
        getQuery(ownerId).then((response) => {
          setOwner(response.data);
        });
      }
    };

    if (walk) {
      fetchOwnerInfo();
    }
  }, [getQuery, walk, isAuthenticated, getAccessTokenSilently]);

  useEffect(() => {
    const fetchWalk = () => {
      getQuery(`walks/${id}`)
          .then((response) => {
            const walk: Walk = response.data;
            walk.walk_date = moment(walk.walk_date);
            setWalk(walk);
          })
          .catch((error: AxiosError) => {
            setError(error.message);
          });
    };
    fetchWalk();
  }, [getQuery, id]);

  if (error) {
    toast.error("Impossible de récupérer les informations de la balade", {
      position: "bottom-right",
      autoClose: 3000,
      hideProgressBar: true,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      toastId: "get-walk-infos-error",
    });
    return <Loader />;
  }
  function description(value: any){
    if(value) {
      return (
          <Grid item xs={12}>
            <Card>
              <Typography m={2} variant="h3" fontSize={"23px"}>
                Description:
              </Typography>
              <CardContent>
                {walk ? (
                    <Typography align={"center"} variant="subtitle1">
                      {walk?.description}
                    </Typography>
                ) : (
                    <Typography align={"center"} variant="subtitle1">
                      <Skeleton />
                    </Typography>
                )}
              </CardContent>
            </Card>
          </Grid>
      )
    }
  }
  const position = [
    walk?.latitude ?? 0,
    walk?.longitude ?? 0,
  ] as LatLngExpression;
  const numberOfAcceptedParticipants = walk?.walk_subscriptions?.reduce(
      (acceptedTotal, walk_subscriptions) =>
          walk_subscriptions.accepted === true ? ++acceptedTotal : acceptedTotal,
      0
  );
  const duration = moment.duration(walk?.walk_duration, "minutes");
  const hours = Math.floor(duration.asHours());
  const minutes = duration.minutes();
  const durationString =
      hours > 0
          ? `${hours}h${minutes.toString().padStart(2, "0")}`
          : `${minutes}min`;
  return (
      <>
        {walk && <WalkActions walk={walk} />}
        <Grid
            container
            spacing={2}
            sx={{ marginTop: "20px", marginBottom: "40px" }}
        >
          <Grid item xs={12}>
            <Card>
              <CardContent>
                <Grid
                    container
                    alignItems="center"
                    justifyContent={"center"}
                    spacing={2}
                >
                  <Grid item xs={6} sm={3}>
                    <Typography variant="subtitle1" color="textSecondary">
                      {t("walk.walk_type.label")}
                    </Typography>
                    <div style={{ display: "flex", alignItems: "center" }}>
                      <Snowshoeing
                          color="primary"
                          style={{ marginRight: "8px" }}
                      />
                      {walk ? (
                          <Typography variant="body1" component="p">
                            {walk?.walk_type
                                ? t(`walk.walk_type.${walk.walk_type}`)
                                : t("walk.walk_type.undefined")}
                          </Typography>
                      ) : (
                          <Skeleton width={"50%"} />
                      )}
                    </div>
                  </Grid>
                  <Grid item xs={6} sm={3}>
                    <Typography variant="subtitle1" color="textSecondary">
                      {t("walk.walk_date")}
                    </Typography>
                    <div style={{ display: "flex", alignItems: "center" }}>
                      <CalendarTodayIcon
                          color="primary"
                          style={{ marginRight: "8px" }}
                      />
                      {walk ? (
                          <Typography variant="body1" component="p">
                            {moment(walk?.walk_date as string).format("llll")}
                            <br />
                          </Typography>
                      ) : (
                          <Skeleton width={"50%"} />
                      )}
                    </div>
                  </Grid>
                  <Grid item xs={6} sm={3}>
                    <Typography variant="subtitle1" color="textSecondary">
                      {t("walk.walk_duration")}
                    </Typography>
                    <div style={{ display: "flex", alignItems: "center" }}>
                      <QueryBuilderIcon
                          color="primary"
                          style={{ marginRight: "8px" }}
                      />
                      {walk ? (
                          <Typography variant="body1" component="p">
                            {durationString}
                          </Typography>
                      ) : (
                          <Skeleton width={"50%"} />
                      )}
                    </div>
                  </Grid>
                  <Grid item xs={6} sm={3}>
                    <Typography variant="subtitle1" color="textSecondary">
                      {t("walk.participants")}
                    </Typography>
                    <div style={{ display: "flex", alignItems: "center" }}>
                      <PeopleIcon
                          color="primary"
                          style={{ marginRight: "8px" }}
                      />
                      {walk ? (
                          <Typography variant="body1" component="p">
                            {numberOfAcceptedParticipants} / {walk.participants_max}
                          </Typography>
                      ) : (
                          <Skeleton width={"50%"} />
                      )}
                    </div>
                  </Grid>
                </Grid>
              </CardContent>
            </Card>
          </Grid>
          {description(walk?.description)}
          <Grid item md={6} xs={12}>
            <Card>
              <CardHeader title={t("walk.position")} />
              <CardContent>
                {walk ? (
                    <MapContainer
                        center={position}
                        zoom={13}
                        style={{ height: "400px" }}
                    >
                      <TileLayer
                          attribution='&copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors'
                          url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                      />
                      <Marker position={position} />
                    </MapContainer>
                ) : (
                    <Skeleton
                        variant="rectangular"
                        animation="wave"
                        width={"100%"}
                        height={400}
                    />
                )}
              </CardContent>
            </Card>
          </Grid>
          <Grid item md={6} xs={12}>
            <>
              <Card>
                <CardHeader
                    avatar={
                      owner ? (
                          <Avatar
                              sx={{ bgcolor: red[500] }}
                              src={owner.picture}
                              aria-label="recipe"
                          />
                      ) : (
                          <Skeleton variant="circular">
                            <Avatar />
                          </Skeleton>
                      )
                    }
                    title={
                      owner ? (
                          owner?.username
                      ) : (
                          <Skeleton width="100%">
                            <Typography>.</Typography>
                          </Skeleton>
                      )
                    }
                />
                <CardContent style={{ height: "433px" }}>
                  {owner ? (
                      <UserGraph
                          userExtraData={owner.user_extra_data as UserExtraData}
                      />
                  ) : (
                      <Skeleton
                          variant="rectangular"
                          animation="wave"
                          width={"100%"}
                          height={400}
                      />
                  )}
                </CardContent>

              </Card>
            </>
          </Grid>
          <Grid item xs={12}>
            <Card>
              <CardHeader title={t("walk.participants")} />
              <CardContent>
                {walk && walk.walk_subscriptions?.length && walk?.owner ? (
                    <WalkSubscribers
                        walkSubscriptions={walk.walk_subscriptions}
                        setWalkSubscriptions={updateWalkSubscriptions}
                        owner={walk?.owner as UserExtraData}
                        maxParticipants={walk.participants_max as number}
                    />
                ) : (
                    <Grid container alignItems="center" spacing={2}>
                      <Grid item xs={12} sm={3} justifyContent="center">
                        <Grid
                            container
                            spacing={2}
                            alignItems="center"
                            justifyContent="center"
                        >
                          <Grid item>
                            <Skeleton variant="circular">
                              <Avatar sx={{ width: 150, height: 150 }} />
                            </Skeleton>
                          </Grid>
                        </Grid>
                      </Grid>

                      <Grid item xs={12} sm={3}>
                        <Grid container spacing={2} alignItems="center">
                          <Grid item>
                            <PetsIcon
                                sx={{ fontSize: "2.5rem", color: "#6b7280" }}
                            />
                          </Grid>
                          <Grid item>
                            <Typography
                                variant="subtitle2"
                                sx={{ fontWeight: "bold" }}
                            >
                              <Skeleton width={200} />
                            </Typography>
                          </Grid>
                        </Grid>

                        <Grid container spacing={2} alignItems="center">
                          <Grid item>
                            <GenderIcon
                                sx={{ fontSize: "2.5rem", color: "#6b7280" }}
                            />
                          </Grid>
                          <Grid item>
                            <Typography
                                variant="subtitle2"
                                sx={{ fontWeight: "bold" }}
                            >
                              <Skeleton width={200} />
                            </Typography>
                          </Grid>
                        </Grid>

                        <Grid container spacing={2} alignItems="center">
                          <Grid item>
                            <BloodtypeIcon
                                sx={{ fontSize: "2.5rem", color: "#6b7280" }}
                            />
                          </Grid>
                          <Grid item>
                            <Typography
                                variant="subtitle2"
                                sx={{ fontWeight: "bold" }}
                            >
                              <Skeleton width={200} />
                            </Typography>
                          </Grid>
                        </Grid>

                        <Grid container spacing={2} alignItems="center">
                          <Grid item>
                            <CalendarTodayIcon
                                sx={{ fontSize: "2.5rem", color: "#6b7280" }}
                            />
                          </Grid>
                          <Grid item>
                            <Typography
                                variant="subtitle2"
                                sx={{ fontWeight: "bold" }}
                            >
                              <Skeleton width={200} />
                            </Typography>
                          </Grid>
                        </Grid>
                      </Grid>

                      <Grid item xs={12} sm={3}>
                        <Grid
                            container
                            spacing={2}
                            direction={"column"}
                            justifyContent={"center"}
                            alignItems={"flex-end"}
                        >
                          <Grid item xs={12}>
                            <Button size="small" color="primary">
                              <Skeleton width={100} />
                            </Button>
                          </Grid>
                        </Grid>
                      </Grid>
                    </Grid>
                )}
              </CardContent>
            </Card>
          </Grid>
        </Grid>
      </>
  );
};

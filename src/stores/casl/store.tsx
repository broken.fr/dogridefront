import React, {Context, createContext, ReactNode, useCallback, useContext, useEffect, useMemo, useState} from 'react';
import {createContextualCan, useAbility} from '@casl/react';
import {AxiosConnection} from "../../lib/AxiosConnection";
import {Ability, AbilityBuilder, AbilityClass, ForcedSubject} from '@casl/ability';
import {useAuth0} from '@auth0/auth0-react';
import {SplashScreen} from "../../components/assets/SplashScreen";
import {User} from "../../interfaces/user";
import {Permission} from "../../interfaces/permission";

export const actions = ['manage'] as const;
export const subjects = ['all'] as const;
export type AppAbilities = [typeof actions[number], typeof subjects[number] | ForcedSubject<Exclude<typeof subjects[number], 'all'>>];
export type AppAbility = Ability<AppAbilities>;
// export const AppAbility = Ability as AbilityClass<AppAbility>;
export const definePermissions = (roles: Permission[]): AppAbility => {
    const ability = Ability as AbilityClass<any>;
    const {can, build} = new AbilityBuilder(ability);
    roles.forEach((val) => {
        can(val.action, val.subject);
    });

    return build();
}

const AbilityContext = createContext(definePermissions([]));
const UserContext = createContext<User | undefined>(undefined);
const Can = createContextualCan(AbilityContext.Consumer);

interface Props {
    children: ReactNode;
}

interface role {
    name: string
}

const AbilityContextProvider = ({children}: Props) => {

    const {getQuery} = AxiosConnection();
    const [roles, setRoles] = useState<Permission[]>(undefined!);
    const [user, setUser] = useState<User>();
    const {
        isLoading,
        loginWithRedirect,
        isAuthenticated
    } = useAuth0();
    const ability = useMemo(() => {
        return roles ? definePermissions(roles) : undefined!;
    }, [roles]);

    const getUserInfos = useCallback(() => {
        if (!user) {
            getQuery('users/me')
                .then((res) => {
                    setUser(res.data);
                    const tempRoles = res.data.permissions ?? [];
                    res.data.roles.forEach((item: role) => {
                        tempRoles.push({
                            action: 'isGranted', subject: item.name
                        });
                    });
                    setRoles(tempRoles);
                })
                .catch((err) => console.log(err));
        }
    }, [user, getQuery])

    useEffect(() => {
        if (!isLoading) {
            if (!isAuthenticated) {
                loginWithRedirect()
            }
            getUserInfos()
        }
    }, [isLoading, getUserInfos, isAuthenticated, loginWithRedirect]);

    return <AbilityContext.Provider value={ability}>{
        ability ?
            <UserContext.Provider value={user}>
                {children}
            </UserContext.Provider>
            :
            <SplashScreen/>
    }</AbilityContext.Provider>;
};

const useAbilityContext = () => useAbility(AbilityContext as Context<any>);

// Generate a hook to use the organization context
const useUserContext = () => useContext(UserContext);

export {AbilityContext, UserContext, Can, AbilityContextProvider, useAbilityContext, useUserContext};